ALTER TABLE Orders ADD COLUMN update_type INTEGER;
ALTER TABLE Customers RENAME TO Customer
ALTER TABLE Orders RENAME TO OrderDetail
ALTER TABLE ServiceProviders RENAME TO ServiceProvider
ALTER TABLE EndServiceProviders RENAME TO EndServiceProvider
ALTER TABLE Pincodes RENAME TO Pincode
ALTER TABLE Categories RENAME TO Category
ALTER TABLE Client RENAME TO Clients
ALTER TABLE Countries RENAME TO Country
ALTER TABLE Users RENAME TO User
ALTER TABLE Fields RENAME TO Field
ALTER TABLE Partners RENAME TO Partner
ALTER TABLE PaymentModes RENAME TO PaymentMode
ALTER TABLE ProductPartners RENAME TO ProductPartner
ALTER TABLE ServiceProviderPartners RENAME TO ServiceProviderPartner
ALTER TABLE ServiceTypes RENAME TO ServiceType
ALTER TABLE States RENAME TO State