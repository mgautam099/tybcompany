package com.wetfiretech.tybapp.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.wetfiretech.tybapp.R
import com.wetfiretech.tybapp.fragment.OrderEntryFragment
import com.wetfiretech.tybapp.model.Customer

class CustomerSearchAdapter internal constructor(private val customers: List<Customer>, private val listener: OrderEntryFragment.OnCustomerSelectedListener) : RecyclerView.Adapter<CustomerSearchAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_customer_search, parent, false)
        val vh = ViewHolder(view)
        view.setOnClickListener { listener.customerSelected(vh.customer!!) }
        return vh
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val sp = customers[position]
        holder.customer = sp
        holder.spName.text = sp.name
        holder.spPhone.text = sp.phone_no
    }

    override fun getItemCount(): Int {
        return customers.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var spName: TextView = itemView.findViewById(R.id.tv_c_name)
        var spPhone: TextView = itemView.findViewById(R.id.tv_c_phone)
        var customer: Customer? = null


    }
}