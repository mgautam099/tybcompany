package com.wetfiretech.tybapp.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.wetfiretech.tybapp.R
import com.wetfiretech.tybapp.fragment.ServiceProviderFragment
import com.wetfiretech.tybapp.model.ServiceProvider
import de.hdodenhof.circleimageview.CircleImageView

/**
 * @author Mayank Gautam
 * Created: 17/09/17
 */

class ServiceProviderAdapter internal constructor(private val serviceProviders: List<ServiceProvider>, private val listener: ServiceProviderFragment.OnInteractionListener) : RecyclerView.Adapter<ServiceProviderAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_service_provider, parent, false)
        val vh = ViewHolder(view)
        view.setOnClickListener { listener.onServiceProviderSelected(vh.serviceProvider) }
        return vh
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val sp = serviceProviders[position]
        holder.serviceProvider = sp
        holder.spName.text = sp.name
        holder.spPhone.text = sp.phone_no

        //TODO add glide implementation
    }

    override fun getItemCount(): Int {
        return serviceProviders.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var spName: TextView = itemView.findViewById(R.id.tv_sp_name)
        var spPhone: TextView = itemView.findViewById(R.id.tv_sp_phone)
        var civProfile: CircleImageView = itemView.findViewById(R.id.ivAvatarImage)
        var serviceProvider: ServiceProvider? = null


    }
}
