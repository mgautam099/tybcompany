package com.wetfiretech.tybapp.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.wetfiretech.tybapp.fragment.ProductFragment
import com.wetfiretech.tybapp.model.Product

class ProductAdapter internal constructor(private val productList: List<Product>, private val listener: ProductFragment.OnInteractionListener) : RecyclerView.Adapter<ProductAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(android.R.layout.simple_list_item_1, parent, false)
        val vh = ViewHolder(view)
        view.setOnClickListener { listener.onProductSelected(vh.product!!) }
        return vh
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val product = productList[position]
        holder.product = product
        holder.pName.text = product.name
    }

    override fun getItemCount(): Int {
        return productList.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var pName: TextView = itemView.findViewById(android.R.id.text1)
        var product: Product? = null
    }
}