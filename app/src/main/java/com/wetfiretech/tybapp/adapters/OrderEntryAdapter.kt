package com.wetfiretech.tybapp.adapters

import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.util.SparseBooleanArray
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.wetfiretech.tybapp.R
import com.wetfiretech.tybapp.custom.CircleView
import com.wetfiretech.tybapp.fragment.OrderListFragment
import com.wetfiretech.tybapp.model.OrderDetail
import com.wetfiretech.tybapp.network.NetworkRequest

class OrderEntryAdapter(private val mValues: List<OrderDetail>, private val mListener: OrderListFragment.OnOrderListInteractionListener?) : RecyclerView.Adapter<OrderEntryAdapter.ViewHolder>() {
    private var mHighlighted = SparseBooleanArray()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_order_entry, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.mItem = mValues[position]
        holder.mIdView.text = mValues[position].complain_no
        holder.mProductView.text = holder.mItem.product?.name
        holder.mCustNameView.text = holder.mItem.customer?.name
        holder.mPhoneNoView.text = holder.mItem.customer?.phone_no
        holder.mServiceView.text = holder.mItem.service_provider?.name + " - " + holder.mItem.service_provider?.phone_no
        var date = holder.mItem.complain_date_time?.subSequence(0,10)?.split("-")
        if(date?.size==3){
            holder.mAddedOnView.text =String.format("%s/%s/%s",date[2],date[1],date[0])
        }
        else
            holder.mAddedOnView.text = "-/-/-"
        if(NetworkRequest.loginDetail?.client_type==1){
            holder.mAddedByView.text = "Registered By: " + holder.mItem.client?.name
        }
        else{
            holder.mAddedByView.visibility = View.GONE
        }
        holder.setStatus(holder.mItem.orderStatusEnum)
        val b = mHighlighted.get(holder.mItem.eid)
        holder.mView.isActivated = b
        holder.mView.setOnClickListener {
            mListener?.onOrderClicked(holder.mItem,position)
        }
    }

    override fun getItemCount(): Int {
        return mValues.size
    }

    fun highLightOrder(order: Int) {
        mHighlighted.put(order, true)
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        val mView: View = view.findViewById(R.id.rl_order_entry)
        val mIdView: TextView = mView.findViewById(R.id.tv_job_id)
        val mCustNameView: TextView = mView.findViewById(R.id.tv_cust_name)
        val mPhoneNoView: TextView = mView.findViewById(R.id.tv_phone_no)
        val mProductView: TextView = mView.findViewById(R.id.tv_product)
        val mServiceView: TextView = mView.findViewById(R.id.et_service_provider)
        val mStatusView: CircleView = mView.findViewById(R.id.cv_order_status)
        val mAddedOnView: TextView = mView.findViewById(R.id.tv_added_on)
        val mAddedByView: TextView = mView.findViewById(R.id.tv_added_by)
        lateinit var mItem: OrderDetail

        override fun toString(): String {
            return super.toString() + " '" + mProductView.text + "'"
        }

        fun setStatus(orderDetailStatus: OrderDetail.OrderStatus?) {
            when (orderDetailStatus) {
                OrderDetail.OrderStatus.SERVICED -> mStatusView.setColor(Color.GREEN)
                OrderDetail.OrderStatus.ASSIGNED -> mStatusView.setColor(Color.YELLOW)
                OrderDetail.OrderStatus.REGISTERED -> mStatusView.setColor(Color.RED)
            }
        }

    }
}
