package com.wetfiretech.tybapp

import android.app.ProgressDialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.os.Handler
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.activeandroid.query.Select
import com.crashlytics.android.Crashlytics
import com.wetfiretech.tybapp.Utils.*
import com.wetfiretech.tybapp.fragment.*
import com.wetfiretech.tybapp.model.*
import com.wetfiretech.tybapp.network.NetworkRequest
import com.wetfiretech.tybapp.network.RetrofitClient
import io.fabric.sdk.android.Fabric
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.placeholder.*
import okhttp3.ResponseBody
import retrofit2.HttpException
import java.io.IOException
import java.net.SocketTimeoutException


class MainActivity : AppCompatActivity(), OnLoadFinishListener,
        OrderListFragment.OnOrderListInteractionListener, OrderEntryFragment.OnInteractionListener,
        ProfileFragment.OnProfileListener, LoginFragment.LoginFragmentInteractionListener {

    private lateinit var orderDetail: OrderDetail

    private var doubleBackPressed = false

    private var isLoggedIn = false

    var notificationReceiver = NotificationReceiver()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Fabric.with(this, Crashlytics())
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        NetworkRequest.loginDetail = Utils.populateLoginDetails(this)
        if(NetworkRequest.loginDetail!=null){
            displayHomeFragment()
        }
        else{
            initLoginFragment()
            if(getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE).contains(KEY_OLD_ACCESS_TOKEN)){
                clearData()
                showSnack(placeholder,"Session expired, please login again!")
            }
        }
    }

    private fun initLoginFragment() {
        val fr = LoginFragment()
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.add(R.id.fragment, fr)
        fragmentTransaction.commitAllowingStateLoss()
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        menu.findItem(R.id.action_sync).isVisible = isLoggedIn
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_sync -> {syncAll()
                true}
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun replaceFragment(fragment: Fragment, addToBackStack: Boolean) {
        val fragmentManager = supportFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.fragment, fragment)
        if (addToBackStack)
            fragmentTransaction.addToBackStack(fragment.toString())
        fragmentTransaction.commitAllowingStateLoss()
    }

    fun getCurrentFragment(): Fragment {
        return supportFragmentManager.findFragmentById(R.id.fragment)
    }

    private fun showSnack(view: View, message: String) {
        try {
            Snackbar.make(view, message, Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()
        } catch (ignored: Exception) {

        }

    }

    override fun getContext(): Context = this

    override fun displayHomeFragment() {
        isLoggedIn= true
        invalidateOptionsMenu()
        val fr = HomeFragment()
        replaceFragment(fr,false)
    }

    var progressView: ProgressDialog? = null
    override fun showProgress(message: String) {
        if(progressView == null)
        {
            progressView = ProgressDialog(this)
            progressView?.isIndeterminate = true
            progressView?.setMessage(message)
            progressView?.setCancelable(false)
        }
        progressView?.show()
    }

    override fun hideProgress() {
        if(progressView!=null)
        {
            progressView?.dismiss()
            progressView = null
        }
    }

//    private fun checkDataLoading(result: ArrayList<Model>) {
//        loadingProgress+=1
//        ActiveAndroid.beginTransaction()
//        try {
//            for(model in result){
//                model.syncOrder()
//            }
//            ActiveAndroid.setTransactionSuccessful()
//        }
//        finally {
//            ActiveAndroid.endTransaction()
//        }
//        if(loadingProgress>=4){
//
//        }
//    }

    private val SUCCESS_MSG_LOGIN: String = "Successfully logged in!"

    private fun successfulLogin(result: LoginDetail) {
        NetworkRequest.loginDetail = result
        showSnack(findViewById(R.id.placeholder), SUCCESS_MSG_LOGIN)
        replaceFragment(MainActivityFragment(),false)
        NetworkRequest.requestOrders(this)
    }

    override fun onOrderClicked(item: OrderDetail, orderIndex: Int) {
        if(orderIndex==-1){
            (getCurrentFragment() as HomeFragment).navigateTo(R.id.action_add)
        }
        else{
            (getCurrentFragment() as HomeFragment).partner = item.partner
            (getCurrentFragment() as HomeFragment).navigateTo(R.id.action_add)
            (getCurrentFragment() as HomeFragment).orderEntryFragment.restore(item,orderIndex)
        }
    }

    private fun truncate(models: List<Model>){
        for(model in models){
            model.truncate(model::class.java)
        }

    }

    private fun clearData(){
//        OrderDetail().truncate(OrderDetail::class.java)
//        Model().truncate(EndServiceProvider::class.java)
//        Customer().truncate(Customer::class.java)
//        Product().truncate(Product::class.java)
//        ServiceProvider().truncate(ServiceProvider::class.java)
//        Pincode().truncate(Pincode::class.java)
//        Category().truncate(Category::class.java)

        val databaseNamev1 = "Application.db"
        val databaseNamev2 ="TYBAppDB"
        val arr = arrayListOf(OrderDetail(),Customer(),Client(),EUser(),Field(),EndServiceProvider(),ServiceProviderPartner(),ServiceProvider(),ProductPartner(),Product(),PaymentMode(),ServiceType(),Category(),Pincode(), State(),Country(),Partner())
        truncate(arr)
        val isDeleted = deleteDatabase(databaseNamev1)
        var rg = getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE).getString(KEY_REGISTRATION_TOKEN,"")
        var url = getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE).getString(KEY_BASE_URL, DEFAULT_BASE_URL)
        getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE).edit().clear().apply()
        getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE).edit().putString(KEY_REGISTRATION_TOKEN,rg).putString(KEY_BASE_URL,url).apply()
    }

    private fun restartActivity(){
        clearData()
        val intent = intent
        finish()
        startActivity(intent)
        overridePendingTransition(0, 0)
    }

    fun logout() {
        val builder = AlertDialog.Builder(this)
        builder.setMessage("Would you like to logout?")
        builder.setPositiveButton("Yes", { _, _ ->
            restartActivity()
        })
        builder.setNegativeButton("No", null)
        val appCompatDialog = builder.create()
        appCompatDialog.show()
    }

    override fun onLogoutRequested() {
        logout()
    }


    override fun onBackPressed() {
        val fr = getCurrentFragment()
        if(fr is HomeFragment){
            if(fr.mPager.currentItem == 2){
                fr.navigateTo(R.id.action_list)
                return
            }
        }
        if (supportFragmentManager.backStackEntryCount == 0) {
            if (doubleBackPressed) {
                super.onBackPressed()
            }
            showSnack(findViewById(R.id.placeholder), "Press back again to exit")
            doubleBackPressed = true

            Handler().postDelayed({ doubleBackPressed = false }, 3000)
        } else {
            super.onBackPressed()
        }
    }

    fun syncAll(){
        val syncList = Select().from(OrderDetail::class.java).where("update_type != ?", OrderDetail.OrderUpdateType.NO_UPDATE).execute<OrderDetail>()
        for(order in syncList){
            syncOrder(order)
        }
        NetworkRequest.getLog(this)
    }


    override fun onSaveRequested(orderDetail: OrderDetail) {
        orderDetail.customer?.save()
        orderDetail.save()
        syncOrder(orderDetail)
        (getCurrentFragment() as HomeFragment).addOrder(orderDetail)
    }

    private fun syncOrder(orderDetail: OrderDetail) {
        when(orderDetail.updateType){
            OrderDetail.OrderUpdateType.DELETE ->{NetworkRequest.deleteOrder(orderDetail,this)
                showSnack(placeholder,"Deleting Complaint")}
            OrderDetail.OrderUpdateType.PUT ->{NetworkRequest.updateOrder(orderDetail,this)
                showSnack(placeholder,"Updating Complaint")}
            OrderDetail.OrderUpdateType.POST -> {NetworkRequest.postOrder(orderDetail, this)
                showSnack(placeholder,"Adding Complaint")}
            OrderDetail.OrderUpdateType.RECREATE ->{NetworkRequest.reCreateOrder(orderDetail,this)
                showSnack(placeholder,"Updating Complaint")}
        }
    }


    override fun onError(throwable: Throwable) {
        try{
            hideProgress()
            Utils.logException(throwable)
            if(getCurrentFragment() is MainActivityFragment){
                restartActivity()
            }
            var ne = getErrorBody(throwable)
            if(getCurrentFragment() is LoginFragment && ne.descr==null && ne.code==403){
                ne.descr = "Invalid username/password"
            }
            if(ne.descr==null)
                ne.descr = "Some error occurred please try again after sometime"
            showSnack(placeholder, ne.descr)
        }
        catch (e: Exception){
            Utils.logException(throwable)
            showSnack(placeholder, "Some error occurred, please try again after sometime.")
        }

    }

    private fun getErrorBody(t: Throwable): NetworkError {
        val e = NetworkError()
        if (t is SocketTimeoutException) {
            e.name = NetworkError.SOCKET_TIMEOUT
            e.descr = "Connection Timed out, please check your internet connection and try again."
        } else if(t is HttpException){
            val r = t.response().errorBody()
            var err = errorResult(r)
            if(err==null || err.descr==null){
                when(t.code()){
                    500 -> e.descr = "Some problem occurred on the server, please try again after sometime"
                    else -> t.message
                }
                e.code = t.code()
            }
            else
                return err
        }
        else {
            e.name = t.message
            e.descr = t.localizedMessage
        }
        return e
    }


    private fun errorResult(errorBody: ResponseBody?) :  NetworkError?{
        val converter = RetrofitClient.getClient(Utils.getBaseUrl(getContext()), getContext())
                .responseBodyConverter<NetworkError>(NetworkError::class.java, arrayOfNulls(0))
        return try {
            if(errorBody!=null){
                val err: NetworkError = converter.convert(errorBody)
                err
            }
            else
                null
        } catch (e: IOException) {
            null
        }

    }

    override fun onLoadComplete(result: Any?, message: String) {
        showSnack(findViewById(R.id.placeholder), message)
        val homeFr = getCurrentFragment() as HomeFragment
        if (result is OrderDetail) {
            homeFr.orderListFragment.notifyDataSetChanged(result)
            homeFr.orderEntryFragment.notifyDataSetChanged()
            homeFr.mPager.adapter.notifyDataSetChanged()
        } else if (result is String) {
            when (result) {
                RESULT_SYNC_COMPLETE -> {
                    homeFr.orderListFragment.checkUpdated()
                    homeFr.orderEntryFragment.notifyDataSetChanged()
                }
            }
        }
    }

    override fun saveToken(loginDetail: LoginDetail) {
        hideProgress()
        val sharedPrefs = getSharedPreferences(Utils.SHARED_PREFS, Context.MODE_PRIVATE)
        val editor = sharedPrefs.edit()
        NetworkRequest.loginDetail?.saveToPrefs(editor)
        editor.apply()
        successfulLogin(loginDetail)
    }

    class NotificationReceiver: BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            if(intent?.action == Utils.NOTIFICATION_INTENT){
                val message = intent.extras?.getBundle(Utils.ARGS_EXTRAS)?.getString(KEY_MODEL_UPDATE)
                Toast.makeText(context,message,Toast.LENGTH_SHORT).show()
                ((context as? MainActivity)?.getCurrentFragment() as? HomeFragment)?.orderListFragment?.checkUpdated()
            }
        }
    }

    override fun onRegistrationRequested() {
        replaceFragment(RegisterFragment(), true)
    }

    override fun onResume() {
        super.onResume()
        registerReceiver(notificationReceiver, IntentFilter(Utils.NOTIFICATION_INTENT))
    }

    override fun onPause() {
        super.onPause()
        unregisterReceiver(notificationReceiver)
    }

}
