package com.wetfiretech.tybapp.fragment

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.EditText
import com.wetfiretech.tybapp.R
import com.wetfiretech.tybapp.Utils
import com.wetfiretech.tybapp.adapters.CustomerSearchAdapter
import com.wetfiretech.tybapp.model.*
import com.wetfiretech.tybapp.network.NetworkRequest
import kotlinx.android.synthetic.main.fragment_complain_detail.*
import java.text.SimpleDateFormat
import java.util.*

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [OrderEntryFragment.OnInteractionListener] interface
 * to handle interaction events.
 * Use the [OrderEntryFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class OrderEntryFragment : Fragment(), ServiceProviderFragment.OnInteractionListener, ProductFragment.OnInteractionListener {

    private val productList = Product().allProducts
    private var selectedProduct: Product? = null
    private var mListener: OnInteractionListener? = null

    private var partner: Partner? = null

    private val spdfr by lazy {
        ServiceProviderFragment.newInstance()
    }

    private val productFragment by lazy {
        ProductFragment()
    }
    private var selectedServiceProvider: ServiceProvider? = null
    private var selectedState: State? = null

    private var customerList = Utils.getAll(Customer::class.java)
    private var filteredCustList = ArrayList<Customer>()

    private var orderDetail: OrderDetail? = null
    private var stateList = Utils.getAll(State::class.java)
    private var partnerList = Utils.getAll(Partner::class.java)
    private lateinit var mAdapter: CustomerSearchAdapter
    private var orderIndex = -1

    private var isCustomerSelected = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mAdapter = CustomerSearchAdapter(filteredCustList, listener);
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater!!.inflate(R.layout.fragment_complain_detail, container, false)
        view.findViewById<View>(R.id.et_product_name).setOnClickListener { displayProductDialog() }
        view.findViewById<View>(R.id.et_service_provider).setOnClickListener { displayServiceProviderDialog() }
        view.findViewById<View>(R.id.et_state).setOnClickListener { displayStateDialog() }
        view.findViewById<View>(R.id.fab_save).setOnClickListener { onSaveButtonPressed() }
        view.findViewById<View>(R.id.rl_entry).setOnClickListener({ _ -> hideRecyclerView(true) })
        val recyclerView = view.findViewById<RecyclerView>(R.id.rv_cust_search)
        recyclerView.layoutManager = LinearLayoutManager(view.context)
        recyclerView.adapter = mAdapter
        val recyclerViewBG = view.findViewById<View>(R.id.backgroundViewRC)
        recyclerViewBG.setOnClickListener({ _ -> hideRecyclerView(true) })
        val custPhone = view.findViewById<EditText>(R.id.et_cust_phone)
        custPhone.onFocusChangeListener = View.OnFocusChangeListener({ _, b ->
            if (!b) {
                hideRecyclerView(true)
                if(!filteredCustList.isEmpty())
                    listener.customerSelected(filteredCustList[0])

            } else {
                if (!filteredCustList.isEmpty())
                    hideRecyclerView(false)
            }
        })
        custPhone.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if ((p0 == null || p0.length < 10)) {
                    isCustomerSelected = false
                    enableViews()
                }
                if (isCustomerSelected && (p0 == null || p0.length >= 10))
                    return
                filteredCustList.clear()
                if (customerList != null && p0 != null) {
                    customerList!!
                            .filter { !p0.isNullOrEmpty() && it.phone_no.startsWith(p0) }
                            .forEach { filteredCustList.add(it) }
                }
                hideRecyclerView(filteredCustList.isEmpty())
                mAdapter.notifyDataSetChanged()
            }
        })
        return view
    }

    public fun showSelectPartnerDialog() {
        if (partnerList.size > 1) {
            val builder: AlertDialog.Builder = AlertDialog.Builder(context)
            builder.setAdapter(ArrayAdapter<Partner>(context, android.R.layout.simple_list_item_1, partnerList), { _, i -> selectPartner(partnerList[i]) })
            builder.setTitle("Select Partner")
            builder.setCancelable(false)
            builder.show()
        } else {
            selectPartner(partnerList[0])
        }
    }

    fun selectPartner(partner: Partner) {
        if(this.partner == partner)
            return
        this.partner = partner
        (parentFragment as HomeFragment).partner = partner
        productFragment.notifyPartnerChanged(partner)
        spdfr.notifyPartnerChanged(partner)

    }

    private fun hideRecyclerView(hide: Boolean) {
        rv_cust_search.visibility = if (hide) View.GONE else View.VISIBLE
        backgroundViewRC.visibility = if (hide) View.GONE else View.VISIBLE

    }

    private fun displayProductDialog() {
//        val builder: AlertDialog.Builder = AlertDialog.Builder(context)
//        builder.setAdapter(ArrayAdapter<Product>(context,android.R.layout.simple_list_item_1,productList), { _, i -> selectProduct(i) })
//        builder.setTitle("Select Product")
//        builder.show()
        if (!productFragment.isAdded)
            productFragment.show(childFragmentManager, "productDialog")
    }

    var states: Array<String>? = null

    private fun displayStateDialog() {

//        if(states == null)
//            states = resources.getStringArray(R.array.states)
        val builder: AlertDialog.Builder = AlertDialog.Builder(context)
        builder.setAdapter(ArrayAdapter<State>(context, android.R.layout.simple_list_item_1, stateList), { _, i -> selectState(stateList[i]) })
        builder.setTitle("Select State")
        builder.show()
    }

    private fun displayServiceProviderDialog() {
        if (!spdfr.isAdded) {
            spdfr.show(childFragmentManager, "complaintDialog")
        }
    }

    private fun selectState(state: State) {
        if (selectedState?.eid != state.eid) {
            selectServiceProvider(null)
        }
        selectedState = state
        et_state?.setText(selectedState?.stateName)
        et_state?.error = null
//        if(selectedState!=null)
        spdfr.notifyDataSetChanged(selectedState)
    }

    internal var listener: OrderEntryFragment.OnCustomerSelectedListener = object : OrderEntryFragment.OnCustomerSelectedListener {

        override fun customerSelected(customer: Customer) {
            isCustomerSelected = true
            filteredCustList.clear()
            hideRecyclerView(true)
            restore(customer, false)
        }
    }

    override fun onServiceProviderSelected(serviceProvider: ServiceProvider?) {
        val order = orderDetail
        //In case status is assigned
        if(order?.orderStatusEnum == OrderDetail.OrderStatus.ASSIGNED){
            if(order.service_provider != serviceProvider){
                til_esp_phone.visibility = View.GONE
            }
            else{
                til_esp_phone.visibility = View.VISIBLE
            }
        }
        selectServiceProvider(serviceProvider)
        spdfr.dismiss()
    }

    fun selectServiceProvider(serviceProvider: ServiceProvider?) {
        selectedServiceProvider = serviceProvider
        if (serviceProvider != null)
            et_service_provider?.setText(serviceProvider.name)
        else
            et_service_provider?.text = null
        et_service_provider?.error = null
    }

    override fun onProductSelected(product: Product) {
        selectedProduct = product
        et_product_name?.setText(selectedProduct?.name)
        et_product_name?.error = null
        productFragment.clear()
        productFragment.dismiss()
    }

    fun onSaveButtonPressed() {
        if (validate()) {
            var order = orderDetail
            if (order == null) {
                order = OrderDetail()
            }
            if(order.eid==-1)
                order.updateType = OrderDetail.OrderUpdateType.POST
            else
                order.updateType = OrderDetail.OrderUpdateType.PUT
            var cust = Customer.getCustomerfromPhone(et_cust_phone.text.toString())

            if (cust == null) {
                cust = Customer()
            }
            cust.name = et_cust_name?.text.toString()
            cust.phone_no = et_cust_phone?.text.toString()
            cust.address = et_address?.text.toString()
            cust.state = selectedState
            cust.setPincode(et_pin?.text.toString())
            cust.save()
            order.customer = cust
            order.product = selectedProduct
            order.in_Warranty = rg_warranty?.checkedRadioButtonId == R.id.rb_in_warranty
            order.issue_description = et_issue_description?.text.toString()
            order.partner = partner
            if(order.client==null){
                order.client = NetworkRequest.loginDetail!!.client
                order.client.eid = NetworkRequest.loginDetail!!.client.eid
            }
            if(order.complain_date_time==null){
                val date = Calendar.getInstance().time
                val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss",Locale.US)
                order.complain_date_time = sdf.format(date)
            }
            if (order.timestamp == null) {
                val tsLong = System.currentTimeMillis() / 1000
                order.timestamp = tsLong.toString()
            }

            //This case will only occur if SP has been tinkered with
            if(order.service_provider!=null && order.service_provider != selectedServiceProvider){
                order.updateType = OrderDetail.OrderUpdateType.RECREATE
                order.orderStatusEnum = OrderDetail.OrderStatus.REGISTERED
                order.end_service_provider = null
            }
            order.service_provider = selectedServiceProvider
            mListener?.onSaveRequested(order)
        }
    }

    fun restore(orderDetail: OrderDetail, index: Int) {
        orderIndex = index
        this.orderDetail = orderDetail
        selectedProduct = orderDetail.product
        et_product_name?.setText(orderDetail.product?.name)
//        if(orderDetail.customer?.state == null && orderDetail.customer?.stateIndex != null && orderDetail.customer?.stateIndex !=1){
//            orderDetail.customer?.state = resources.getStringArray(R.array.states)[orderDetail.customer?.stateIndex!!]
//        }
        rg_warranty?.check(if (orderDetail.in_Warranty) R.id.rb_in_warranty else R.id.rb_out_warranty)
        et_issue_description?.setText(orderDetail.issue_description)
        lockFields(orderDetail.orderStatusEnum != OrderDetail.OrderStatus.SERVICED)
        restore(orderDetail.customer!!, orderDetail.orderStatusEnum == OrderDetail.OrderStatus.SERVICED)
        selectServiceProvider(orderDetail.service_provider)
        et_service_provider?.isEnabled = orderDetail.orderStatusEnum != OrderDetail.OrderStatus.SERVICED
        sp_drop?.visibility = if (et_service_provider.isEnabled) View.VISIBLE else View.GONE
        if(orderDetail.orderStatusEnum != OrderDetail.OrderStatus.REGISTERED){
            et_esp_phone.setText(orderDetail.end_service_provider.phone_no)
            til_esp_phone.visibility = View.VISIBLE
        }
        else{
            til_esp_phone.visibility = View.GONE
        }
        if(orderDetail.orderStatusEnum == OrderDetail.OrderStatus.SERVICED){
            til_amount_paid.visibility = View.VISIBLE
            et_amount_paid.setText(orderDetail.paidAmount.toString())
            if(!orderDetail.complain_resolve_comment.isNullOrBlank()){
                til_complain_comment.visibility = View.VISIBLE
                et_resolve_comment.setText(orderDetail.complain_resolve_comment)
            }
            else{
                til_complain_comment.visibility = View.GONE
            }
        }
        else{
            til_amount_paid.visibility = View.GONE
            til_complain_comment.visibility = View.GONE
        }

    }

    fun restore(customer: Customer, lockFields: Boolean) {
        isCustomerSelected = true
        et_cust_name?.setText(customer.name)
        et_cust_phone?.setText(customer.phone_no)
        et_address?.setText(customer.address)
        et_pin?.setText(customer.pincode?.pincode)
        selectState(customer.state)
        if (!lockFields) {
            et_cust_name?.isEnabled = et_cust_name.text.isEmpty()
            et_address?.isEnabled = et_address.text.isEmpty()
            et_pin?.isEnabled = et_pin.text.isEmpty()
            et_state?.isEnabled = et_state.text.isEmpty()
            state_drop?.visibility = if (et_state.isEnabled) View.VISIBLE else View.GONE
        }
    }

    fun enableViews() {
        et_cust_name?.isEnabled = true
        et_address?.isEnabled = true
        et_pin?.isEnabled = true
        et_state?.isEnabled = true
        state_drop?.visibility = View.VISIBLE
    }

    fun lockFields(isEnabled: Boolean) {
        et_cust_phone?.isEnabled = isEnabled;
        et_cust_name?.isEnabled = isEnabled;
        et_address?.isEnabled = isEnabled;
        et_pin?.isEnabled = isEnabled;
        et_state?.isEnabled = isEnabled;
        state_drop?.visibility = if (isEnabled) View.VISIBLE else View.GONE
        rg_warranty?.isEnabled = isEnabled;
        rb_in_warranty?.isEnabled = isEnabled;
        rb_out_warranty?.isEnabled = isEnabled;
        et_product_name?.isEnabled = isEnabled;
        product_drop?.visibility = if (isEnabled) View.VISIBLE else View.GONE
        et_issue_description?.isEnabled = isEnabled
        et_service_provider?.isEnabled = isEnabled
        sp_drop?.visibility = if (isEnabled) View.VISIBLE else View.GONE
        fab_save?.visibility = if (isEnabled) View.VISIBLE else View.GONE
    }

    private fun validate(): Boolean {
        var toReturn = true
        if (selectedProduct == null) {
            //Snackbar.make(placeholder,"Please select a product",Snackbar.LENGTH_SHORT)
            et_product_name?.error = "Please select a product"
            toReturn = false
        }
        if (selectedServiceProvider == null) {
            //Snackbar.make(placeholder,"Please select a serviceType provider",Snackbar.LENGTH_SHORT)
            et_service_provider?.error = "Please select a serviceType provider"
            toReturn = false
        }
        if (et_cust_name != null && et_cust_name.text.isEmpty()) {
            et_cust_name.error = "Customer name cannot be empty"
            toReturn = false
        }
        val text = et_cust_phone?.text.toString()
        if (text.isEmpty() || (
                !text.startsWith("9") &&
                        !text.startsWith("8") &&
                        !text.startsWith("7") &&
                        !text.startsWith("6") &&
                        !text.startsWith("0")) || text.length < 10) {
            et_cust_phone?.error = "Enter a valid Customer Number"
            toReturn = false
        }
        if (et_address != null && et_address.text.isEmpty()) {
            et_address?.error = "Address cannot be empty"
            toReturn = false
        }
        if (selectedState == null) {
            et_state?.error = "Please select a state"
            toReturn = false
        }
        if (et_pin != null && et_pin.text.isNullOrBlank() || et_pin.text.length != 6) {
            et_pin?.error = "Please enter a valid pin"
            toReturn = false
        }
        return toReturn
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OnInteractionListener) {
            mListener = context
        } else {
            throw RuntimeException(context!!.toString() + " must implement OnInteractionListener")
        }
    }

    //testing

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }


    fun isDirty(): Boolean {
        if (orderDetail?.orderStatusEnum == OrderDetail.OrderStatus.SERVICED)
            return false
        if (selectedProduct != null && selectedProduct?.eid != orderDetail?.product?.eid)
            return true
        if (selectedServiceProvider != null && selectedServiceProvider?.eid != orderDetail?.service_provider?.eid)
            return true
        if (!et_cust_name?.text.isNullOrBlank() && et_cust_name?.text.toString() != orderDetail?.customer?.name)
            return true
        if (!et_cust_phone?.text.isNullOrBlank() && et_cust_phone?.text.toString() != orderDetail?.customer?.phone_no)
            return true
        if (!et_address?.text.isNullOrBlank() && et_address?.text.toString() != orderDetail?.customer?.address)
            return true
        if (!et_issue_description?.text.isNullOrBlank() && et_issue_description?.text.toString() != orderDetail?.issue_description)
            return true
        if (!et_pin?.text.isNullOrBlank() && et_pin?.text.toString() != orderDetail?.customer?.pincode?.pincode)
            return true
        if (selectedState != null && selectedState != orderDetail?.customer?.state)
            return true
        return false
    }

    fun refresh() {
        orderDetail = null
        et_cust_name?.clear()
        et_cust_phone?.clear()
        selectedProduct = null
        et_product_name?.clear()
        et_address?.clear()
        productFragment.clear()
        selectedState = null
        spdfr.clear()
        et_state?.clear()
        et_pin?.clear()
        selectServiceProvider(null)
        et_issue_description?.clear()
        rg_warranty?.clearCheck()
    }


    interface OnInteractionListener {
        fun onSaveRequested(orderDetail: OrderDetail)
    }

    interface OnCustomerSelectedListener {
        fun customerSelected(customer: Customer)
    }

    fun notifyDataSetChanged() {
       customerList= Utils.getAll(Customer::class.java)
    }

}

private fun EditText.clear() {
    text = null
    error = null
    clearFocus()
}
