package com.wetfiretech.tybapp.fragment

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.wetfiretech.tybapp.BuildConfig
import com.wetfiretech.tybapp.Utils

import com.wetfiretech.tybapp.databinding.FragmentProfileBinding
import com.wetfiretech.tybapp.network.NetworkRequest

class ProfileFragment : Fragment() {

    private var mListener: OnProfileListener? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val binding = FragmentProfileBinding.inflate(inflater,container, false)
        binding.client = NetworkRequest.loginDetail?.client
        binding.buttonLogout.setOnClickListener({_ -> mListener?.onLogoutRequested()})
        val footerText = StringBuilder("v").append(BuildConfig.VERSION_NAME)
        if(Utils.getBaseUrl(context) != Utils.DEFAULT_BASE_URL)
            footerText.append(" - ").append(Utils.getBaseUrl(context))
        binding.footerVersion.text = footerText
        return binding.root
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OnProfileListener) {
            mListener = context
        } else {
            throw RuntimeException(context!!.toString() + " must implement OnProfileListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    interface OnProfileListener {
        fun onLogoutRequested()
    }

}// Required empty public constructor
