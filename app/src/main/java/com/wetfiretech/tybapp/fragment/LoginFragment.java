package com.wetfiretech.tybapp.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.wetfiretech.tybapp.OnLoadFinishListener;
import com.wetfiretech.tybapp.R;
import com.wetfiretech.tybapp.Utils;
import com.wetfiretech.tybapp.network.NetworkRequest;
import com.wetfiretech.tybapp.network.RetrofitClient;

public class LoginFragment extends Fragment implements TextView.OnEditorActionListener{
    public static final int AUTHORISED = 2;
    public static final int INVALID = 3;
    TextInputEditText mETPassword;
    TextInputEditText mETUsername;
    TextView mTVErrorPass;
    TextView mTVErrorUser;
    int devCount = 0;
    boolean devEnabled;
    Handler handler = new Handler();

    LoginFragmentInteractionListener mListener;

    public LoginFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        mETUsername = view.findViewById(R.id.etUsername);
        mETPassword = view.findViewById(R.id.etPassword);
        mTVErrorPass = view.findViewById(R.id.tv_pass_error);
        mTVErrorUser = view.findViewById(R.id.tv_username_error);
        mETPassword.setOnEditorActionListener(this);
        view.findViewById(R.id.dev_access).setOnClickListener(this::checkDevAccess);
        view.findViewById(R.id.buttonLogin).setOnClickListener(this::checkLogin);
        view.findViewById(R.id.button_register).setOnClickListener(v -> this.openRegistration());
        return view;
    }

    private void checkDevAccess(View v) {
        devCount++;
        getActivity().invalidateOptionsMenu();
        if(devCount==7){
            if(!devEnabled) {
                getActivity().invalidateOptionsMenu();
                Toast.makeText(getContext(), "You have now enabled dev menu", Toast.LENGTH_SHORT).show();
            }
            else
                Toast.makeText(getContext(), "Dev menu has already been activated", Toast.LENGTH_SHORT).show();
            devEnabled = true;
        }
        else if(devCount==1)
            handler.postDelayed(() -> devCount = 0, 3000);
    }

    public void checkLogin(View v) {
        String username = mETUsername.getText().toString();
        String password = mETPassword.getText().toString();

        if (password.isEmpty()){
            mTVErrorPass.setVisibility(View.VISIBLE);
        }
        else{
            mTVErrorPass.setVisibility(View.INVISIBLE);
        }

        if (username.isEmpty())
            mTVErrorUser.setVisibility(View.VISIBLE);
        else
            mTVErrorUser.setVisibility(View.INVISIBLE);

        switch (checkUsername(username, password)) {
            //  case NEW_LOGIN: showRegistration(username,password); break;
            case AUTHORISED:
                login(username, password);
                break;
            default:
                break;
        }
    }

    private void openRegistration() {
        if (mListener != null)
            mListener.onRegistrationRequested();
    }

    private void login(String username, String password) {
        SharedPreferences sharedPreferences = getContext().getSharedPreferences(Utils.SHARED_PREFS, Context.MODE_PRIVATE);
        String registrationToken = sharedPreferences.getString(Utils.KEY_REGISTRATION_TOKEN,"");
        NetworkRequest.INSTANCE.generateNewToken(username, password, registrationToken,(OnLoadFinishListener) getActivity());
    }

    private int checkUsername(String username, String pass) {
        if (!username.isEmpty() && !pass.isEmpty()) {
            return AUTHORISED;
        }

        return INVALID;
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (v.getId() == R.id.etPassword && actionId == EditorInfo.IME_ACTION_GO) {
            checkLogin(v);
            return true;
        }
        return false;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if(devEnabled)
            inflater.inflate(R.menu.dev_menu,menu);
        super.onCreateOptionsMenu(menu, inflater);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.action_settings){
            displayUrlChangeView();
        }
        return super.onOptionsItemSelected(item);
    }

    private void displayUrlChangeView() {

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        View view = this.getLayoutInflater().inflate(R.layout.item_simple_edittext,null);
        builder.setView(view);
        EditText etUrlChange = view.findViewById(R.id.et_url);
        builder.setMessage("Please enter new URL:");
        etUrlChange.setText(Utils.getBaseUrl(getContext()));
        builder.setPositiveButton("OK", (dialogInterface, i) -> {
            dialogInterface.dismiss();
            String url = etUrlChange.getText().toString().trim();
            if(url.length()>10){
                if(!url.endsWith("/"))
                    url+="/";
                if(!url.equals(Utils.getBaseUrl(getContext()))){
                    Utils.setBaseUrl(url,getContext());
                    RetrofitClient.resetService();
                }
            }
            else
            {
                Toast.makeText(getContext(), "Please enter a valid url", Toast.LENGTH_SHORT).show();
            }
        });
        builder.setNegativeButton("CANCEL",null);
        builder.show();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof LoginFragmentInteractionListener) {
            mListener = (LoginFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement LoginFragmentListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mETPassword = null;
        mETUsername = null;
        mTVErrorPass = null;
        mTVErrorUser = null;
    }

    public interface LoginFragmentInteractionListener {
        void onRegistrationRequested();
    }
}
