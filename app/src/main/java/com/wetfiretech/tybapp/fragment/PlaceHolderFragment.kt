package com.wetfiretech.tybapp.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.wetfiretech.tybapp.R


/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [OnInteractionListener] interface
 * to handle interaction events.
 * Use the [PlaceHolderFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class PlaceHolderFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        var view = inflater!!.inflate(R.layout.fragment_complain, container, false)
        // Inflate the layout for this fragment
        return view
    }

}// Required empty public constructor
