package com.wetfiretech.tybapp.fragment

import android.app.AlertDialog
import android.os.Bundle
import android.support.annotation.IdRes
import android.support.design.internal.BottomNavigationItemView
import android.support.design.internal.BottomNavigationMenuView
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.activeandroid.query.Select
import com.wetfiretech.tybapp.R
import com.wetfiretech.tybapp.custom.CustomViewPager
import com.wetfiretech.tybapp.model.OrderDetail
import com.wetfiretech.tybapp.model.Partner


/**
 * @author Mayank Gautam
 * Created: 20/09/17
 */

class HomeFragment : Fragment() {


    lateinit var orderListFragment: OrderListFragment
    lateinit var orderEntryFragment: OrderEntryFragment

    private lateinit var bottomNavigationView: BottomNavigationView

    lateinit var mPager: CustomViewPager

    var discardData = false
    var partner: Partner? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater!!.inflate(R.layout.fragment_home, container, false)
        mPager = view.findViewById(R.id.homePager)
        val mAdapter = HomePagerAdapter(childFragmentManager)
        orderListFragment = OrderListFragment()
        orderEntryFragment = OrderEntryFragment()
        mAdapter.addFragment(orderListFragment)
        mAdapter.addFragment(AnalysisFragment())
        mAdapter.addFragment(orderEntryFragment)
        mAdapter.addFragment(ProfileFragment())
        bottomNavigationView = view.findViewById(R.id.bnv_main)
        bottomNavigationView.setOnNavigationItemSelectedListener { item -> when(item.itemId){
            R.id.action_list -> {
                                    if(discardData){
                                        mPager.setCurrentItem(INDEX_ORDERLIST_FRAGMENT,false)
                                        partner = null
                                    }else
                                        checkOrderAdding(R.id.action_list)
                                    discardData}
            R.id.action_add -> {
                discardData = false
                mPager.setCurrentItem(INDEX_COMPLAIN_DETAIL_FRAGMENT,false)
                if(partner==null){
                    orderEntryFragment.showSelectPartnerDialog()
                }
                else{
                    orderEntryFragment.selectPartner(partner!!)
                }
                true}

            R.id.action_analysis -> {
                mPager.setCurrentItem(INDEX_ANALYSIS_FRAGMENT, false)
                true
            }
            R.id.action_profile -> {
                mPager.setCurrentItem(INDEX_PROFILE_FRAGMENT, false)
                true
            }
            else -> false
        }
        }
        disableShiftMode(bottomNavigationView)
        mPager.adapter = mAdapter
        mPager.setPagingEnabled(false)
        return view
    }

    private fun checkOrderAdding(@IdRes action: Int){
        if(mPager.currentItem == 2 && orderEntryFragment.isDirty() && !discardData){
            AlertDialog.Builder(context).setMessage("Edited data will be discarded?")
                    .setPositiveButton("Yes", { _, _ ->    discardData = true
                                                                orderEntryFragment.refresh()
                                                                bottomNavigationView.selectedItemId = action})
                    .setNegativeButton("No", { _, _ ->     discardData = false})
                    .show()
        }
        else{
            discardData = true
            orderEntryFragment.refresh()
            bottomNavigationView.selectedItemId = action

        }
    }

    private fun getOrders() : List<OrderDetail>?{
        return Select().from(OrderDetail::class.java).orderBy("id DESC").execute() ?: arrayListOf()

    }

    public fun addOrder(orderDetail: OrderDetail){
        //if(orderDetail.eid==-1)
        orderListFragment.addOrder(orderDetail)
        discardData = true
        orderEntryFragment.refresh()
        bottomNavigationView.selectedItemId = R.id.action_list
    }

    class HomePagerAdapter(fragmentManager: FragmentManager) : FragmentPagerAdapter(fragmentManager) {

        var fragmentList = arrayListOf<Fragment>()
        override fun getItem(position: Int): Fragment {
            return fragmentList[position]
        }

        override fun getCount(): Int {
            return fragmentList.size
        }

        fun addFragment(fragment: Fragment){
            fragmentList.add(fragment)
        }
    }
    fun setPage(index: Int){
        mPager.setCurrentItem(index, true)
    }

    companion object {
        val INDEX_ORDERLIST_FRAGMENT = 0
        val INDEX_ANALYSIS_FRAGMENT = 1
        val INDEX_COMPLAIN_DETAIL_FRAGMENT = 2
        val INDEX_PROFILE_FRAGMENT = 3
    }

    fun navigateTo(action: Int){
        bottomNavigationView.selectedItemId = action
    }

    override fun onResume() {
        super.onResume()
        orderListFragment.updateOrders()
    }

    // Method for disabling ShiftMode of BottomNavigationView
    private fun disableShiftMode(view: BottomNavigationView) {
        val menuView = view.getChildAt(0) as BottomNavigationMenuView
        try {
            val shiftingMode = menuView.javaClass.getDeclaredField("mShiftingMode")
            shiftingMode.isAccessible = true
            shiftingMode.setBoolean(menuView, false)
            shiftingMode.isAccessible = false
            for (i in 0 until menuView.childCount) {
                val item = menuView.getChildAt(i) as BottomNavigationItemView
                item.setShiftingMode(false)
                // set once again checked value, so view will be updated
                item.setChecked(item.itemData.isChecked)
            }
        } catch (e: NoSuchFieldException) {
            Log.e("BNVHelper", "Unable to get shift mode field", e)
        } catch (e: IllegalAccessException) {
            Log.e("BNVHelper", "Unable to change value of shift mode", e)
        }

    }
}
