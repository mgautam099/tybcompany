package com.wetfiretech.tybapp.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.activeandroid.query.Select;
import com.wetfiretech.tybapp.R;
import com.wetfiretech.tybapp.Utils;
import com.wetfiretech.tybapp.adapters.OrderEntryAdapter;
import com.wetfiretech.tybapp.model.LoginDetail;
import com.wetfiretech.tybapp.model.OrderDetail;
import com.wetfiretech.tybapp.network.NetworkRequest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnOrderListInteractionListener}
 * interface.
 */
public class OrderListFragment extends Fragment {

    private static final String ARG_JOB_LIST = "job-list";
    private static final String ARG_CURRENT_SEARCH = "curr-search";

    private static final int FILTER_CUSTOMER = 0;
    private static final int FILTER_CUSTOMER_PHONE = 1;
    private static final int FILTER_PRODUCT = 2;
    private static final int FILTER_SERVICE_PROVIDER =3;
    private static final int FILTER_REGISTERED_BY = 4;
    private static final String[] filterOrderStatus = {"Registered", "Assigned", "Serviced"};
    OrderEntryAdapter mAdapter;
    private String[] filters = {"Customer Name", "Customer Phone", "Product Name", "Service Provider Name", "Registered By"};
    private boolean[] filterOrders = new boolean[]{true,true,true};
    private OnOrderListInteractionListener mListener;
    private ArrayList<OrderDetail> mFilteredOrderDetailList;
    private ArrayList<OrderDetail> mAllOrderDetails;
    private EditText orderSearcher;
    private int filterIndex = -1;
    TextWatcher searchTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            int textLength = orderSearcher.getText().length();
            mFilteredOrderDetailList.clear();
            try {
                for (OrderDetail orderDetail : mAllOrderDetails) {
                    String name = "";
                    switch (filterIndex){
                        case FILTER_CUSTOMER: if(orderDetail.customer!=null) name = orderDetail.customer.name;
                            break;
                        case FILTER_CUSTOMER_PHONE: if(orderDetail.customer!=null) name = orderDetail.customer.phone_no; break;
                        case FILTER_PRODUCT: if(orderDetail.product!=null)name = orderDetail.product.name;break;
                        case FILTER_SERVICE_PROVIDER: if(orderDetail.service_provider!=null) name = orderDetail.service_provider.name;break;
                        case FILTER_REGISTERED_BY: if(orderDetail.client!=null) name = orderDetail.client.name;break;
                        default:
                            name = "";
                    }

                    assert name != null;
                    if (textLength <= name.length()) {
                        if (name.toLowerCase().contains(orderSearcher.getText().toString().toLowerCase().trim())) {
                            if(filterOrders[orderDetail.getOrderStatus()])
                                mFilteredOrderDetailList.add(orderDetail);
                        }
                    }
                }
                mAdapter.notifyDataSetChanged();
            } catch (Exception e) {
                Utils.logException(e);
            }
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    };
    private TextView filterCountView;
    private String currentSearch = null;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public OrderListFragment() {
    }

    public static OrderListFragment newInstance(ArrayList<OrderDetail> orderDetailList) {
        OrderListFragment fragment = new OrderListFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList(ARG_JOB_LIST, orderDetailList);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(savedInstanceState!=null){
            mAllOrderDetails = savedInstanceState.getParcelableArrayList(ARG_JOB_LIST);
            mFilteredOrderDetailList = new ArrayList<>();
            mFilteredOrderDetailList.addAll(mAllOrderDetails);
            currentSearch = savedInstanceState.getString(ARG_CURRENT_SEARCH);
            //savedInstanceState.getInt(ARG_CURRENT_INDEX,0);
        }
        else {
            mAllOrderDetails = new ArrayList<>();
            getOrders();
            mFilteredOrderDetailList = new ArrayList<>();
            mFilteredOrderDetailList.addAll(mAllOrderDetails);
        }
        LoginDetail loginDetail = NetworkRequest.INSTANCE.getLoginDetail(getContext());
        if(loginDetail!=null && loginDetail.client_type!=1){
            filters = Arrays.copyOf(filters,filters.length-1);
        }
        mAdapter = new OrderEntryAdapter(mFilteredOrderDetailList, mListener);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view;
        if(!mAllOrderDetails.isEmpty()){
            view = inflater.inflate(R.layout.fragment_company_home, container, false);
            RecyclerView recyclerView = view.findViewById(R.id.list);
            Context context = view.getContext();
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
            recyclerView.setAdapter(mAdapter);
            view.findViewById(R.id.iv_filter).setOnClickListener(this::showFilter);
            orderSearcher = view.findViewById(R.id.et_order_search);
            filterCountView = view.findViewById(R.id.tv_filter_count);
            updateFilterCount();
            view.findViewById(R.id.fab_filter).setOnClickListener(this::showOrderStatusFilter);
            if(currentSearch!=null)
                orderSearcher.setText(currentSearch);
            if(filterIndex == -1){
                orderSearcher.setOnTouchListener((view1, event) -> {
                    if(event.getAction() != MotionEvent.ACTION_DOWN)
                        return false;
                    showFilter(view1);
                    return true;
                });
            }
            orderSearcher.addTextChangedListener(searchTextWatcher);
        }
        else{
            view = inflater.inflate(R.layout.fragment_init_order, container, false);
            view.setOnClickListener(v -> mListener.onOrderClicked(new OrderDetail(),-1));
        }
        return view;
    }

    private void updateFilterCount() {
        int count = 0;
        for(boolean filter: filterOrders){
            if(filter) count++;
        }
        filterCountView.setText(""+count);
        if(count==0)
            filterCountView.setVisibility(View.INVISIBLE);
        else
            filterCountView.setVisibility(View.VISIBLE);
    }

    private void showFilter(View v) {
        if(v.getId() == R.id.iv_filter || filterIndex == -1)
        new AlertDialog.Builder(getContext()).setSingleChoiceItems(filters, filterIndex, (dialogInterface, i) -> {
            if(filterIndex != i){
                orderSearcher.setText(null);
                orderSearcher.setOnTouchListener(null);
                orderSearcher.requestFocus();
                filterIndex = i;
                orderSearcher.setInputType(filterIndex==1 ? InputType.TYPE_CLASS_PHONE : InputType.TYPE_CLASS_TEXT);
            }
            dialogInterface.dismiss();
        }).show();
    }

    private void showOrderStatusFilter(View v) {
        final boolean[] prevFilters = filterOrders.clone();
        new AlertDialog.Builder(getContext())
                .setMultiChoiceItems(filterOrderStatus, filterOrders, (dialogInterface, i, b) -> filterOrders[i]=b)
                .setPositiveButton("OK", (dialogInterface, i) -> {
                    orderSearcher.setText(orderSearcher.getText());
                    updateFilterCount();
                    dialogInterface.dismiss();})
                .setNegativeButton("Cancel", (dialogInterface, i) -> filterOrders = prevFilters)
                .setOnCancelListener(dialogInterface -> filterOrders = prevFilters).show();
    }

    @Override
    public void onResume() {
        super.onResume();
        checkUpdated();
    }

    public void updateOrders(){
        if(mAllOrderDetails !=null){
            mAllOrderDetails.clear();
            getOrders();
            if(orderSearcher!=null)
                orderSearcher.setText(orderSearcher.getText());
        }
    }

    public void addOrder(OrderDetail orderDetail){
        if(orderDetail ==null)
            return;
        replaceOrUpdate(orderDetail);
        if(orderSearcher!=null)
            orderSearcher.setText(orderSearcher.getText());
    }

    public void replaceOrUpdate(OrderDetail orderDetail){
        for(int i = 0; i< mAllOrderDetails.size(); i++){
            OrderDetail orderDetail1 = mAllOrderDetails.get(i);
            if(orderDetail.getId().equals(orderDetail1.getId())){
                mAllOrderDetails.remove(i);
                mAllOrderDetails.add(i, orderDetail);
                return;
            }
        }
        mAllOrderDetails.add(0, orderDetail);
    }

    public void notifyDataSetChanged(OrderDetail orderDetail) {
        if(orderDetail.updateType == OrderDetail.OrderUpdateType.DELETE){
            for(int i = 0; i< mAllOrderDetails.size(); i++){
                OrderDetail orderDetail1 = mAllOrderDetails.get(i);
                if(orderDetail.getId().equals(orderDetail1.getId())){
                    mAllOrderDetails.remove(i);
                    break;
                }
            }
        }
        mAdapter.notifyDataSetChanged();
        orderDetail.updateType = OrderDetail.OrderUpdateType.NO_UPDATE;
        orderDetail.save();
        if(orderSearcher!=null)
            orderSearcher.setText(orderSearcher.getText());
    }

    private void getOrders(){
        if(mAllOrderDetails!=null){
//            for(OrderDetail.OrderStatus os: OrderDetail.OrderStatus.values()){
//                List<OrderDetail> orders1 = new Select().from(OrderDetail.class).where("order_status = ?", os).orderBy("id DESC").execute();
//                if (orders1!=null)
//                    mAllOrderDetails.addAll(orders1);
//            }
            List<OrderDetail> orders = new Select().from(OrderDetail.class).orderBy("id DESC").execute();
            if (orders != null)
                mAllOrderDetails.addAll(orders);
        }
    }

    public void checkUpdated(){
        String updated = Utils.getFromPrefs(Utils.KEY_UPDATED_MODELS,getContext());
        if(!updated.isEmpty()){
            for(String s: updated.split(",")){
                mAdapter.highLightOrder(Integer.parseInt(s));
            }
            updateOrders();
            mAdapter.notifyDataSetChanged();
            Utils.deleteFromPrefs(Utils.KEY_UPDATED_MODELS, getContext());
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(ARG_JOB_LIST, mAllOrderDetails);
        try{
            outState.putString(ARG_CURRENT_SEARCH, orderSearcher.getText().toString());
        }catch (Exception e){
            Utils.logException(e);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnOrderListInteractionListener) {
            mListener = (OnOrderListInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnOrderListInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        filterCountView = null;
        orderSearcher = null;
    }

    public interface OnOrderListInteractionListener {
        void onOrderClicked(OrderDetail item, int orderIndex);
    }
}
