package com.wetfiretech.tybapp.fragment

import android.content.DialogInterface
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.EditText
import com.wetfiretech.tybapp.R
import com.wetfiretech.tybapp.Utils
import com.wetfiretech.tybapp.adapters.ServiceProviderAdapter
import com.wetfiretech.tybapp.model.Partner
import com.wetfiretech.tybapp.model.ServiceProvider
import com.wetfiretech.tybapp.model.ServiceProviderPartner
import com.wetfiretech.tybapp.model.State


/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [ServiceProviderFragment.OnInteractionListener] interface
 * to handle interaction events.
 * Use the [ServiceProviderFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ServiceProviderFragment : DialogFragment() {

    var serviceProviderList =  ArrayList<ServiceProvider>()
    lateinit var filteredList: ArrayList<ServiceProvider>

    private lateinit var mAdapter: ServiceProviderAdapter

    private var state: State? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        filteredList = ArrayList(serviceProviderList)
    }

    private var searcher: EditText? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater!!.inflate(R.layout.fragment_complain_servprov, container, false)

        dialog.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)
        val recyclerView = view.findViewById<RecyclerView>(R.id.list)
        val layoutManager = LinearLayoutManager(context)
        recyclerView.layoutManager = layoutManager
        val dividerItemDecoration = DividerItemDecoration(recyclerView.context,
                layoutManager.orientation)
        dividerItemDecoration.setDrawable(ContextCompat.getDrawable(activity, R.drawable.divider))
        recyclerView.addItemDecoration(dividerItemDecoration)
        mAdapter = ServiceProviderAdapter(filteredList, parentFragment as OnInteractionListener)
        searcher = view.findViewById(R.id.et_servicep_search)
        searcher!!.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                val textLength = searcher!!.text.length
                filteredList.clear()
                try {
                    for (serviceProvider in serviceProviderList) {
                        val name: String? = serviceProvider.name
                        if (textLength <= name!!.length) {
                            if (name.toLowerCase().startsWith(searcher!!.text.toString().toLowerCase().trim { it <= ' ' })) {
                                if(state==null || serviceProvider.state==null ||state?.eid == serviceProvider.state.eid)
                                    filteredList.add(serviceProvider)
                            }
                        }
                    }
                    mAdapter.notifyDataSetChanged()
                } catch (e: Exception) {
                    Utils.logException(e)
                }

            }

            override fun afterTextChanged(s: Editable) {}
        })
        recyclerView.adapter = mAdapter
        searcher!!.text = null
        return view
    }

    interface OnInteractionListener {
        fun onServiceProviderSelected(serviceProvider: ServiceProvider?)
    }

    fun notifyDataSetChanged(state: State?){
        this.state = state
        if(searcher!=null)
            searcher!!.text = searcher!!.text
    }

    fun notifyPartnerChanged(partner: Partner){
        serviceProviderList.clear()
        serviceProviderList.addAll(ServiceProviderPartner.getAllServiceProviders(partner))
    }

    companion object {
        val ARG_SP = "sp"
        fun newInstance() : ServiceProviderFragment{
            val frag = ServiceProviderFragment()
            return frag
        }
    }

    fun clear() {
        this.state = null
        if(searcher!=null)
            searcher!!.text = null
    }

    override fun onCancel(dialog: DialogInterface?) {
        super.onCancel(dialog)
        if(searcher!=null)
            searcher!!.text = null

    }

    override fun onDismiss(dialog: DialogInterface?) {
        super.onDismiss(dialog)
        if(searcher!=null)
            searcher!!.text = null
    }

}
