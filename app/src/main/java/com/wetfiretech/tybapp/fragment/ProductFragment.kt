package com.wetfiretech.tybapp.fragment

import android.content.DialogInterface
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v4.content.ContextCompat
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.EditText
import android.widget.TextView
import com.crashlytics.android.Crashlytics
import com.wetfiretech.tybapp.R
import com.wetfiretech.tybapp.adapters.ProductAdapter
import com.wetfiretech.tybapp.model.Partner
import com.wetfiretech.tybapp.model.Product
import com.wetfiretech.tybapp.model.ProductPartner
import com.wetfiretech.tybapp.model.ServiceProvider

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [ProductFragment.OnInteractionListener] interface
 * to handle interaction events.
 * Use the [ProductFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ProductFragment : DialogFragment() {

    private var productList =  ArrayList<Product>()
    lateinit var filteredList: ArrayList<Product>

    private lateinit var mAdapter: ProductAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        filteredList = ArrayList(productList)
    }

    private var searcher: EditText? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater!!.inflate(R.layout.fragment_complain_servprov, container, false)
        dialog.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)

        val recyclerView = view.findViewById<RecyclerView>(R.id.list)
        val layoutManager = LinearLayoutManager(context)
        recyclerView.layoutManager = layoutManager
        val dividerItemDecoration = DividerItemDecoration(recyclerView.context,
                layoutManager.orientation)
        dividerItemDecoration.setDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.divider));
        recyclerView.addItemDecoration(dividerItemDecoration)
        view.findViewById<TextView>(R.id.label_select_sp).text = "Select Product"
        mAdapter = ProductAdapter(filteredList, parentFragment as OnInteractionListener)
        searcher = view.findViewById(R.id.et_servicep_search)
        searcher!!.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                val textLength = searcher!!.text.length
                filteredList.clear()
                try {
                    for (product in productList) {
                        var name: String? = product.name
                        if (textLength <= name!!.length) {
                            if (name.toLowerCase().contains(searcher!!.text.toString().toLowerCase().trim { it <= ' ' })) {
                                filteredList.add(product)
                            }
                        }
                    }
                    mAdapter.notifyDataSetChanged()
                } catch (e: Exception) {
                    Crashlytics.logException(e)
                }

            }

            override fun afterTextChanged(s: Editable) {}
        })
        recyclerView.adapter = mAdapter
        return view
    }

    interface OnInteractionListener {
        fun onProductSelected(product: Product)
    }

    companion object {
        val ARG_SP = "sp"
        fun newInstance(serviceProvideList: ArrayList<ServiceProvider>) : ProductFragment {
            val args = Bundle()
            args.putParcelableArrayList(ARG_SP,serviceProvideList)
            val frag = ProductFragment()
            frag.arguments = args
            return frag
        }
    }

    fun clear() {
        if(searcher!=null)
            searcher!!.text = null
    }

    override fun onCancel(dialog: DialogInterface?) {
        clear()
        super.onCancel(dialog)
    }

    fun notifyPartnerChanged(partner: Partner){
        productList.clear()
        productList.addAll(ProductPartner.getAllProducts(partner))
    }

}// Required empty public constructor