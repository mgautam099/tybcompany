package com.wetfiretech.tybapp.network

import android.content.Context
import com.activeandroid.ActiveAndroid
import com.activeandroid.Cache
import com.fasterxml.jackson.databind.ObjectMapper
import com.wetfiretech.tybapp.OnLoadFinishListener
import com.wetfiretech.tybapp.Utils
import com.wetfiretech.tybapp.Utils.*
import com.wetfiretech.tybapp.model.*
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import java.lang.StringBuilder

object NetworkRequest {
    var loginDetail: LoginDetail? = null

    fun getLoginDetail(context: Context): LoginDetail?{
        if (loginDetail==null)
            Utils.populateLoginDetails(context)
        return loginDetail
    }
    private fun <T> getService(service: Class<T>, context: Context): T? {
        return RetrofitClient.getClient(Utils.getBaseUrl(context), context).create(service)
    }

    fun generateNewToken(username: String, password: String,registrationToken: String, listener: OnLoadFinishListener) {
        listener.showProgress("Logging in")
        val apiService = getService(ApiService::class.java, listener.getContext())
        val fields = HashMap<String, String>()
        fields.put(KEY_USERNAME, username)
        fields.put(KEY_PASSWORD, password)
        fields.put(KEY_REGISTRATION,registrationToken)
        apiService!!.login(fields)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ c ->
                    c.username = username
                    loginDetail = c
                    listener.saveToken(c)}, listener::onError)
    }

    fun register(registerDetails: HashMap<String, String>, listener: OnLoadFinishListener) {
        listener.showProgress("Registering User")
        val apiService = getService(ApiService::class.java, listener.getContext())
        apiService!!.register(registerDetails)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ c ->
                    c.username = registerDetails[KEY_USERNAME]
                    loginDetail = c
                    listener.saveToken(c)
                }, listener::onError)
    }

    fun requestOrders(listener: OnLoadFinishListener) {
        val accessToken = "ApiKey " + loginDetail!!.username!! +":"+ loginDetail!!.access_token!!
        val apiService = getService(ApiService::class.java, listener.getContext())
        apiService!!.getPartners(accessToken)
                .subscribeOn(Schedulers.newThread())
                .observeOn(Schedulers.newThread())
                .doOnNext(this::saveModels)
                .flatMap { apiService.getStates(accessToken) }
                .doOnNext(this::saveModels)
                .flatMap { apiService.getPincodes(accessToken) }
                .doOnNext(this::saveModels)
                .flatMap { apiService.getProducts(accessToken) }
                .doOnNext(this::saveModels)
                .flatMap { apiService.getServiceTypes(accessToken) }
                .doOnNext(this::saveModels)
                .flatMap { apiService.getClients(accessToken) }
                .doOnNext(this::saveModels)
                .flatMap { apiService.getServiceProviders(accessToken) }
                .doOnNext(this::saveModels)
                .flatMap { apiService.getCustomers(accessToken) }
                .doOnNext(this::saveModels)
                .flatMap { apiService.getPaymentModes(accessToken) }
                .doOnNext(this::saveModels)
                .flatMap { apiService.getEndServiceProviders(accessToken) }
                .doOnNext(this::saveModels)
                .flatMap { apiService.getOrders(accessToken) }
                .doOnNext(this::saveModels)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({listener.displayHomeFragment()}, listener::onError)
    }

    fun deleteOrder(orderDetail: OrderDetail, listener: OnLoadFinishListener) {
        val accessToken = "ApiKey " + loginDetail!!.username!! +":"+ loginDetail!!.access_token!!
        val apiService = getService(ApiService::class.java, listener.getContext())
        apiService!!.deleteOrder(orderDetail.eid,accessToken)
                .subscribeOn(Schedulers.newThread())
                .doOnNext({ orderDetail.delete()})
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe ({
                    listener.onLoadComplete(orderDetail,"OrderDetail "+ orderDetail.complain_no+" Deleted")},
                        listener::onError)
    }

    fun reCreateOrder(orderDetail: OrderDetail, listener: OnLoadFinishListener) {
        val accessToken = "ApiKey " + loginDetail!!.username!! +":"+ loginDetail!!.access_token!!
        val apiService = getService(ApiService::class.java, listener.getContext())
        apiService!!.deleteOrder(orderDetail.eid,accessToken)
                .subscribeOn(Schedulers.newThread())
                .observeOn(Schedulers.io())
                .flatMap { orderDetail.eid = -1
                    apiService.postOrder(accessToken, orderDetail) }
                .doOnNext { t-> saveModel(t, orderDetail) }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe ({ orderNew ->
                    orderDetail.eid = orderNew.eid
                    //order.customer?.eid = orderNew.customer?.eid!!
                    listener.onLoadComplete(orderDetail,"OrderDetail "+ orderDetail.complain_no+" Updated")},
                        listener::onError)
    }

    fun postOrder(orderDetail: OrderDetail, listener: OnLoadFinishListener){
        val accessToken = "ApiKey " + loginDetail!!.username!! +":"+ loginDetail!!.access_token!!
        val apiService = getService(ApiService::class.java, listener.getContext())
        apiService!!.postOrder(accessToken, orderDetail)
                .subscribeOn(Schedulers.newThread())
                .observeOn(Schedulers.io())
                .doOnNext { t-> saveModel(t, orderDetail) }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe ({ orderNew ->
                    orderDetail.eid = orderNew.eid
                    //order.customer?.eid = orderNew.customer?.eid!!
                    listener.onLoadComplete(orderDetail,"OrderDetail "+ orderDetail.complain_no+" Received")},
                listener::onError)
    }

    fun updateOrder(orderDetail: OrderDetail, listener: OnLoadFinishListener){
        val accessToken = "ApiKey " + loginDetail!!.username!! +":"+ loginDetail!!.access_token!!
        val apiService = getService(ApiService::class.java, listener.getContext())
        apiService!!.updateOrder(orderDetail.eid,accessToken, orderDetail)
                .subscribeOn(Schedulers.newThread())
                .observeOn(Schedulers.io())
                .doOnNext { t-> saveModel(t, orderDetail) }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe ({ orderNew ->
                    orderDetail.eid = orderNew.eid
                    //order.customer?.eid = orderNew.customer?.eid!!
                    listener.onLoadComplete(orderDetail,"OrderDetail "+ orderDetail.complain_no+" Updated")},
                        listener::onError)
    }


    fun getLog(listener: OnLoadFinishListener){
        if(loginDetail==null)
            loginDetail = Utils.populateLoginDetails(listener.getContext())
        if(loginDetail==null)
            return
        val loginDetail = loginDetail
        val fields = HashMap<String, String>()
        fields.put(KEY_APIKEY, loginDetail!!.access_token)
        fields.put(KEY_TIMESTAMP, loginDetail.timestamp)
        val apiService = getService(ApiService::class.java, listener.getContext())
        apiService!!.getLog(fields)
                .subscribeOn(Schedulers.newThread())
                .observeOn(Schedulers.io())
                .doOnNext ({r -> saveLog(r, listener.getContext())})
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe (
                        {listener.onLoadComplete(Utils.RESULT_SYNC_COMPLETE,"Sync complete")},
                        listener::onError)
    }

    private fun saveLog(logs: List<LogWrapper>, context: Context) {
//        val rClient = RetrofitClient.getClient(Utils.getBaseUrl(listener.getContext()),listener.getContext())
        var timestamp: String? = null
        var str = StringBuilder(Utils.getFromPrefs(Utils.KEY_UPDATED_MODELS,context))
        if(str.isNotEmpty())
            str.append(",")
        val objectMapper = ObjectMapper()
        ActiveAndroid.beginTransaction()
        try {
            for(log in logs){
                saveOrDeleteModel(log,objectMapper)
                timestamp = log.log.timestamp
                if(log.log.action!=-1 && log.log.entryTable==Cache.getTableName(OrderDetail::class.java))
                    str.append(log.log.modelId).append(",")
            }
            ActiveAndroid.setTransactionSuccessful()
        } catch (e: Exception){
            Utils.logException(e)
        }
        finally {
            ActiveAndroid.endTransaction()
            if(timestamp!=null)
                loginDetail?.updateTimestamp(timestamp,context)
            if(!str.isEmpty())
                Utils.saveToPrefs(Utils.KEY_UPDATED_MODELS,str.substring(0,str.length-1),context)

        }
    }

    private fun saveOrDeleteModel(log: LogWrapper, objectMapper: ObjectMapper) {
        val c = Cache.getTable(log.log.entryTable)

        when(log.log.action){
            0,1 -> {
                var model = if(log.log.entryTable == Cache.getTableName(OrderDetail::class.java))
                    Utils.getByTimestampModel(c,log.data.get("timestamp").asText())
                else
                    Utils.getByIdModel(c,log.log.modelId)
                model = if(model!=null)
                    objectMapper.readerForUpdating(model).readValue(log.data)
                else
                    objectMapper.treeToValue(log.data, c)
                model.save()
            }
            -1 -> Utils.getByIdModel(c,log.log.modelId)?.delete()
        }
    }

    private fun saveModel(model2: OrderDetail, model: OrderDetail){
        model.eid = model2.eid
        model.complain_no = model2.complain_no
        model.customer?.eid = model2.customer?.eid!!
        model.customer?.name = model2.customer?.name!!
        model.customer?.save()
        model.setClient(model2.client)
        try{
            model.save()
        }
        catch(e: Exception){
            Utils.logException(e)
        }
    }

    private fun saveModels(models: List<com.activeandroid.Model>) {
        ActiveAndroid.beginTransaction()
        try {
            for (model in models) {
                model.save()
            }
            ActiveAndroid.setTransactionSuccessful()
        } catch (e: Exception){
            Utils.logException(e)
        }
        finally {
            ActiveAndroid.endTransaction()
        }
    }

    private fun updateModels(models: List<Model>) {
        ActiveAndroid.beginTransaction()
        try {
            for (model in models) {
                model.save()
            }
            ActiveAndroid.setTransactionSuccessful()
        } catch (e: Exception){
            Utils.logException(e)
        }
        finally {
            ActiveAndroid.endTransaction()
        }
    }

    private fun deleteModels(models: List<com.activeandroid.Model>) {
        ActiveAndroid.beginTransaction()
        try {
            for (model in models) {
                model.delete()
            }
            ActiveAndroid.setTransactionSuccessful()
        } catch (e: Exception){
            Utils.logException(e)
        }
        finally {
            ActiveAndroid.endTransaction()
        }
    }

    private fun saveModels(models: ModelListWrapper<out Model>) {
        saveModels(models.objects)
    }


}
