package com.wetfiretech.tybapp.network;

import com.wetfiretech.tybapp.model.LoginDetail;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface TokenService {

    @POST("/token")
    @FormUrlEncoded
    Call<LoginDetail> getToken(@Field("grant_type") String grant_type,
                               @Field("username") String username,
                               @Field("password") String password);

}
