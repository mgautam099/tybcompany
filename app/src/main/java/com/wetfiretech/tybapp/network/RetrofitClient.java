package com.wetfiretech.tybapp.network;

import android.content.Context;
import android.os.Build;

import com.wetfiretech.tybapp.BuildConfig;
import com.wetfiretech.tybapp.R;
import com.wetfiretech.tybapp.Utils;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit.converter.jackson.JacksonConverterFactory;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;


public class RetrofitClient {
    private static Retrofit retrofit = null;

    public static Retrofit getClient(String baseUrl, final Context context) {
        if (retrofit == null) {
            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            OkHttpClient client;
            builder.connectTimeout(30, TimeUnit.SECONDS);
            builder.addInterceptor(chain -> {
                Request original = chain.request();
                Request newRequest = original.newBuilder()
                        .header("User-Agent", context.getResources().getString(R.string.app_name) + "/" + BuildConfig.VERSION_NAME)
                        .addHeader("Application", BuildConfig.APPLICATION_ID)
                        .addHeader("Client", Utils.getAndroidVersion())
                        .addHeader("Version", BuildConfig.VERSION_NAME)
                        .addHeader("Device", Build.BRAND + " " + Build.MODEL)
                        .build();
                return chain.proceed(newRequest);
            });
            client = builder.build();
            retrofit = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .client(client)
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .addConverterFactory(JacksonConverterFactory.create())
                    .build();

        }
        return retrofit;
    }

    public static void resetService(){
        retrofit = null;
    }


}
