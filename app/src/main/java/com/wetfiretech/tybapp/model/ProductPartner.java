package com.wetfiretech.tybapp.model;

import android.os.Parcelable;

import com.activeandroid.Cache;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.ArrayList;
import java.util.List;

@Table(name = "ProductPartner")
public class ProductPartner extends Model implements Parcelable {

    @Column
    public Product product;

    @Column
    public Partner partner;

    @Column
    public boolean isActive;

    public static List<ProductPartner> allItems(Partner partner){
        return new Select().from(ProductPartner.class).where(Cache.getTableName(ProductPartner.class) + ".partner=?", partner.getId()).execute();
    }


    public static ArrayList<Product> getAllProducts(Partner partner){
        ArrayList<Product> ps = new ArrayList<>();
        List<ProductPartner> pps = new Select().from(ProductPartner.class).where(Cache.getTableName(ProductPartner.class) + ".partner=?", partner.getId()).execute();
        for(ProductPartner pp: pps){
            ps.add(pp.product);
        }
        return ps;
    }
}
