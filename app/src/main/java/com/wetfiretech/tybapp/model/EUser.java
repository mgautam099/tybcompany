package com.wetfiretech.tybapp.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Mayank Gautam
 *         Created: 18/09/17
 */
@Table(name = "User")
public class EUser extends Model implements Parcelable{

    public static final Creator<EUser> CREATOR = new Creator<EUser>() {
        @Override
        public EUser createFromParcel(Parcel in) {
            return new EUser(in);
        }

        @Override
        public EUser[] newArray(int size) {
            return new EUser[size];
        }
    };

    @Column
    @JsonProperty("username")
    public String username;

    public EUser(){
        super();
    }

    protected EUser(Parcel in) {
        super(in);
        username = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(username);
    }
}
