package com.wetfiretech.tybapp.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.wetfiretech.tybapp.Utils;

/**
 * @author Mayank Gautam
 *         Created: 02/10/17
 */

@Table(name = "Customer")
public class Customer extends Person implements Parcelable {
    public static final Creator<Customer> CREATOR = new Creator<Customer>() {
        @Override
        public Customer createFromParcel(Parcel in) {
            return new Customer(in);
        }

        @Override
        public Customer[] newArray(int size) {
            return new Customer[size];
        }
    };
    @Column
    public Partner partner;
    @Column
    @JsonProperty("client")
    public Client client;

    public Customer(){
        super();
    }

    protected Customer(Parcel in) {
        super(in);
        partner = in.readParcelable(Partner.class.getClassLoader());
        client = in.readParcelable(Client.class.getClassLoader());
    }

    @JsonIgnore
    public static Customer getCustomerfromPhone(String phone){
        return new Select().from(Customer.class).where("phone_no = ?",phone).executeSingle();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeParcelable(partner, flags);
        dest.writeParcelable(client, flags);
    }

    @JsonProperty("client")
    public void setClient(Client value) {
        if(value!=null){
            this.client = Utils.getById(Client.class,value.eid);
            if (this.client==null)
            {

                this.client = value;
                this.client.save();
            }
        }

    }

    @JsonProperty("client_id")
    public void setClientId(int client) {
        this.client = Utils.getById(Client.class,client);
    }

    @JsonProperty("partner")
    public void setPartner(Partner partner) {
        this.partner = Utils.getById(Partner.class,partner.eid);
        if(this.partner==null){
            this.partner = partner;
            this.partner.save();
        }
    }

    @JsonProperty("partner_id")
    public void setPartnerId(int partner) {
        this.partner = Utils.getById(Partner.class,partner);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Customer customer = (Customer) o;

        if (partner != null ? !partner.equals(customer.partner) : customer.partner != null)
            return false;
        return client != null ? client.equals(customer.client) : customer.client == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (partner != null ? partner.hashCode() : 0);
        result = 31 * result + (client != null ? client.hashCode() : 0);
        return result;
    }
}
