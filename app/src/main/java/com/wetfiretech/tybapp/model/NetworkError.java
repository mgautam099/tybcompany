package com.wetfiretech.tybapp.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Mayank Gautam
 *         Created: 03/10/17
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class NetworkError implements Parcelable {

    public static final Creator<NetworkError> CREATOR = new Creator<NetworkError>() {
        @Override
        public NetworkError createFromParcel(Parcel in) {
            return new NetworkError(in);
        }

        @Override
        public NetworkError[] newArray(int size) {
            return new NetworkError[size];
        }
    };
    public static String SOCKET_TIMEOUT = "socket_timeout";
    @JsonProperty("error")
    public String name;
    @JsonProperty("error_message")
    public String descr;
    @JsonProperty("code")
    public int code;

    public NetworkError(){
        super();
    }

    protected NetworkError(Parcel in) {
        name = in.readString();
        descr = in.readString();
        code = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(descr);
        dest.writeInt(code);
    }

    @Override
    public int describeContents() {
        return 0;
    }
}
