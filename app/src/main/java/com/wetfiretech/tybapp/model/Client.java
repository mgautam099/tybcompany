package com.wetfiretech.tybapp.model;

import android.content.SharedPreferences;
import android.os.Parcel;
import android.os.Parcelable;

import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.wetfiretech.tybapp.Utils;

/**
 * @author Mayank Gautam
 *         Created: 29/09/17
 */

@Table(name = "Client")
public class Client extends Person implements Parcelable {


    public static final Creator<Client> CREATOR = new Creator<Client>() {
        @Override
        public Client createFromParcel(Parcel in) {
            return new Client(in);
        }

        @Override
        public Client[] newArray(int size) {
            return new Client[size];
        }
    };
//    @Column(name = "partner")
//    private Partner partner;
    @Column
    private EUser user;
    @Column
    private Field field;

    public Client(){
        super();
    }

    protected Client(Parcel in) {
        super(in);
//        partner = in.readParcelable(Partner.class.getClassLoader());
        user = in.readParcelable(EUser.class.getClassLoader());
        field = in.readParcelable(Field.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
//        dest.writeParcelable(partner, flags);
        dest.writeParcelable(user, flags);
        dest.writeParcelable(field, flags);
    }

    @JsonProperty("user")
    public EUser getUser() {
        return user;
    }

    @JsonProperty("user")
    public void setUser(EUser user) {
        this.user = Utils.getById(EUser.class,user.eid);
        if(this.user==null)
        {
            user.save();
            this.user = Utils.getById(EUser.class,user.eid);
        }
    }

    @JsonProperty("field")
    public Field getField() {
        return field;
    }

    @JsonProperty("field")
    public void setField(Field field) {
        this.field = Utils.getById(Field.class,field.eid);
        if(this.field==null)
        {
            field.save();
            this.field = Utils.getById(Field.class,field.eid);
        }
    }

    public void saveToPrefs(SharedPreferences.Editor editor) {
        super.saveToPrefs(editor);
    }

    public void restorePrefs(SharedPreferences sharedPrefs) {
        super.restorePrefs(sharedPrefs);
    }

//    @JsonProperty("partner")
//    public List<Partner> getPartner() {
//        ArrayList<Partner> list = new ArrayList<>();
//        if(partner!=null)
//            list.add(partner);
//        return list;
//    }

//    @JsonProperty("partner")
//    public void setPartner(List<Partner> partners) {
//        if(partners.size()>0)
//        //todo change it to permanent solution
//            this.partner = Utils.getById(Partner.class,partners.get(0).eid);
//        //for (Partner partner :
////                partners) {
////            this.partner.add(partner.getPartner(partner.getEid()));
////        }
//    }

}
