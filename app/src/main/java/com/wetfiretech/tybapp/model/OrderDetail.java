package com.wetfiretech.tybapp.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.wetfiretech.tybapp.Utils;

/**
 * @author Mayank Gautam
 *         Created: 01/10/17
 */

@Table(name = "OrderDetail")
public class OrderDetail extends Model implements Parcelable {
    public static final Creator<OrderDetail> CREATOR = new Creator<OrderDetail>() {
        @Override
        public OrderDetail createFromParcel(Parcel in) {
            return new OrderDetail(in);
        }

        @Override
        public OrderDetail[] newArray(int size) {
            return new OrderDetail[size];
        }
    };
    @Column(name = "complain_no")
    @JsonProperty("complain_no")
    public String complain_no;
    @Column(name = "complain_date_time")
    @JsonProperty("complain_date_time")
    public String complain_date_time;
    @Column(name = "client")
    @JsonProperty("client")
    public Client client;
    @Column(name = "partner")
    @JsonProperty("partner")
    public Partner partner;
    @Column(name = "customer")
    @JsonProperty("customer")
    public Customer customer;
    @Column(name = "paid_amount")
    @JsonProperty("paid_amount")
    public double paidAmount;
    @Column(name = "product")
    @JsonProperty("product")
    public Product product;
    @Column(name = "service_type")
    @JsonProperty("service_type")
    public ServiceType serviceType;
    @Column(name = "service_provider")
    @JsonProperty("service_provider")
    public ServiceProvider service_provider;
    @Column(name = "issue_description")
    @JsonProperty("issue_description")
    public String issue_description;
    @Column(name = "extra_info")
    @JsonProperty("extra_info")
    public String extra_info;
    @Column(name = "payment_mode")
    @JsonProperty("payment_mode")
    public PaymentMode paymentMode;
    @Column(name = "preferred_service_time_max")
    @JsonProperty("preferred_service_time_max")
    public String preferredServiceTimeMax;
    @Column(name = "preferred_service_time_min")
    @JsonProperty("preferred_service_time_min")
    public String preferredServiceTimeMin;
    @Column(name = "quantity")
    @JsonProperty("quantity")
    public int quantity;
    @Column(name = "required_payment_amount")
    @JsonProperty("required_payment_amount")
    public int requiredPaymentAmount;
    @Column(name = "complain_service_date")
    @JsonProperty("complain_service_date")
    public String complain_service_date;
    @Column(name = "complain_resolve_comment")
    @JsonProperty("complain_resolve_comment")
    public String complain_resolve_comment;
    @Column(name = "end_service_provider")
    @JsonProperty("end_service_provider")
    public EndServiceProvider end_service_provider;
    @Column(name = "timestamp")
    @JsonProperty("timestamp")
    public String timestamp;
    @Column(name = "update_type")
    public int updateType = OrderUpdateType.NO_UPDATE;
    @Column(name = "order_status")
    private OrderStatus orderStatus = OrderStatus.REGISTERED;
    @Column(name = "in_warranty")
    private int in_warranty = 0;



    public OrderDetail(){
        super();
        orderStatus = OrderStatus.REGISTERED;
    }


    public OrderDetail(int eid){
    }


    protected OrderDetail(Parcel in) {
        super(in);
        complain_no = in.readString();
        complain_date_time = in.readString();
        client = in.readParcelable(Client.class.getClassLoader());
        partner = in.readParcelable(Partner.class.getClassLoader());
        customer = in.readParcelable(Customer.class.getClassLoader());
        paidAmount = in.readDouble();
        product = in.readParcelable(Product.class.getClassLoader());
        serviceType = in.readParcelable(ServiceType.class.getClassLoader());
        service_provider = in.readParcelable(ServiceProvider.class.getClassLoader());
        issue_description = in.readString();
        extra_info = in.readString();
        paymentMode = in.readParcelable(PaymentMode.class.getClassLoader());
        preferredServiceTimeMax = in.readString();
        preferredServiceTimeMin = in.readString();
        quantity = in.readInt();
        requiredPaymentAmount = in.readInt();
        complain_service_date = in.readString();
        complain_resolve_comment = in.readString();
        end_service_provider = in.readParcelable(EndServiceProvider.class.getClassLoader());
        timestamp = in.readString();
        in_warranty = in.readInt();
        updateType = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(complain_no);
        dest.writeString(complain_date_time);
        dest.writeParcelable(client, flags);
        dest.writeParcelable(partner, flags);
        dest.writeParcelable(customer, flags);
        dest.writeDouble(paidAmount);
        dest.writeParcelable(product, flags);
        dest.writeParcelable(serviceType, flags);
        dest.writeParcelable(service_provider, flags);
        dest.writeString(issue_description);
        dest.writeString(extra_info);
        dest.writeParcelable(paymentMode, flags);
        dest.writeString(preferredServiceTimeMax);
        dest.writeString(preferredServiceTimeMin);
        dest.writeInt(quantity);
        dest.writeInt(requiredPaymentAmount);
        dest.writeString(complain_service_date);
        dest.writeString(complain_resolve_comment);
        dest.writeParcelable(end_service_provider, flags);
        dest.writeString(timestamp);
        dest.writeInt(in_warranty);
        dest.writeInt(updateType);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @JsonProperty("client")
    public void setClient(Client value) {
        if(value!=null){
            client = Utils.getById(Client.class,value.eid);
            if (client==null)
            {
                client = value;
                client.save();

            }
        }

    }

    @JsonProperty("client_id")
    public void setClientId(int client) {
        this.client = Utils.getById(Client.class,client);
    }

    @JsonProperty("partner")
    public void setPartner(Partner partner) {
        this.partner = Utils.getById(partner);
    }

    @JsonProperty("partner_id")
    public void setPartnerId(int partner) {
        this.partner = Utils.getById(Partner.class,partner);
    }

    @JsonProperty("customer")
    public void setCustomer(Customer c){
        if(c!=null && c.eid != -1){
            customer = Customer.getCustomerfromPhone(c.phone_no);
            if(customer!=null)
            {
                if(customer.eid == -1)
                    customer.eid = c.eid;
                if(customer.name== null || !customer.name.equals(c.name))
                    customer.name = c.name;
                if(customer.address== null || !customer.address.equals(c.address))
                    customer.address = c.address;
                if(customer.state== null || !customer.state.equals(c.state))
                    customer.state = c.state;
                if(customer.getPincode()== null || !customer.getPincode().equals(c.getPincode()))
                    customer.setPincode(c.getPincode());
            }
            else{
                customer = c;
            }
            customer.save();
        }
    }

    @JsonProperty("customer_id")
    public void setCustomerId(int customer) {
        this.customer = Utils.getById(Customer.class,customer);
    }

    @JsonProperty("order_status")
    public int getOrderStatus(){
        return orderStatus.ordinal();
    }

    @JsonProperty("order_status")
    public void setOrderStatus(int value) {
        orderStatus = OrderStatus.values()[value];
    }

    @JsonIgnore
    public OrderStatus getOrderStatusEnum(){
        return orderStatus;
    }

    @JsonIgnore
    public void setOrderStatusEnum(OrderStatus orderStatus){
        this.orderStatus = orderStatus;
    }

    @JsonProperty("product")
    public void setProduct(Product product){
        this.product = Utils.getById(product);
    }


    @JsonProperty("product_id")
    public void setProductId(int product){
        this.product = Utils.getById(Product.class,product);
    }

    @JsonProperty("service_type")
    public void setServiceType(ServiceType serviceType) {
        if(serviceType!=null) {
            this.serviceType = Utils.getById(ServiceType.class,serviceType.eid);
        }
    }

    @JsonProperty("service_type_id")
    public void setServiceTypeId(int serviceType) {
        this.serviceType = Utils.getById(ServiceType.class,serviceType);
    }

    @JsonProperty("service_provider")
    public void setService_Provider(ServiceProvider s) {
        if(s != null)
        service_provider = Utils.getById(s);
    }

    @JsonProperty("service_provider_id")
    public void setService_ProviderId(int s) {
        service_provider = Utils.getById(ServiceProvider.class,s);
    }

    @JsonProperty("in_warranty")
    public boolean getIn_Warranty(){
        return in_warranty==1;
    }

    @JsonProperty("in_warranty")
    public void setIn_Warranty(boolean b){
        in_warranty = b?1:0;
    }

    @JsonProperty("payment_mode")
    public void setPaymentMode(PaymentMode value) {
        if(value!=null)
        {
            paymentMode = Utils.getById(value);
        }
    }

    @JsonProperty("payment_mode_id")
    public void setPaymentModeId(int value) {
        paymentMode = Utils.getById(PaymentMode.class,value);
    }

    @JsonProperty("end_service_provider")
    public void setEndServiceProvider(EndServiceProvider s) {
        if(s!=null){
            end_service_provider = Utils.getById(s);
            if(end_service_provider==null){
                end_service_provider = s;
                end_service_provider.save();
            }
        }
    }

    @JsonProperty("end_service_provider_id")
    public void setEndServiceProviderId(int s) {
        end_service_provider = Utils.getById(EndServiceProvider.class,s);
    }

    public void update(OrderDetail orderDetail){
        super.update(orderDetail);
        end_service_provider = orderDetail.end_service_provider;
        customer = orderDetail.customer;
        orderStatus = orderDetail.orderStatus;
    }

    public enum OrderStatus {
        REGISTERED, ASSIGNED, SERVICED
    }

    public abstract class OrderUpdateType{
        public static final int DELETE = -1;
        public static final int NO_UPDATE = 0;
        public static final int PUT = 1;
        public static final int POST = 2;
        public static final int RECREATE = 3;
    }
}
