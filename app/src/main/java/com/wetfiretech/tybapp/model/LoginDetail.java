package com.wetfiretech.tybapp.model;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.wetfiretech.tybapp.Utils;

/**
 * @author Mayank Gautam
 *         Created: 03/10/17
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class LoginDetail implements Parcelable {

    public static final Creator<LoginDetail> CREATOR = new Creator<LoginDetail>() {
        @Override
        public LoginDetail createFromParcel(Parcel in) {
            return new LoginDetail(in);
        }

        @Override
        public LoginDetail[] newArray(int size) {
            return new LoginDetail[size];
        }
    };
    @JsonProperty("AccessToken")
    public String access_token;
    @JsonProperty("timestamp")
    public String timestamp;
    @JsonProperty("token_type")
    public String token_type;
    @JsonProperty("user_id")
    public int userId = -1;
    @JsonProperty("role")
    public String role;
    //1 - admin, 2 - User
    @JsonProperty("client_type")
    public int client_type;
    @JsonProperty("username")
    public String username;
    @JsonProperty("client")
    public Client client;

    public LoginDetail(){
        super();
    }

    protected LoginDetail(Parcel in) {
        access_token = in.readString();
        timestamp = in.readString();
        token_type = in.readString();
        userId = in.readInt();
        role = in.readString();
        client_type = in.readInt();
        username = in.readString();
        client = in.readParcelable(Client.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(access_token);
        dest.writeString(timestamp);
        dest.writeString(token_type);
        dest.writeInt(userId);
        dest.writeString(role);
        dest.writeInt(client_type);
        dest.writeString(username);
        dest.writeParcelable(client, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public void saveToPrefs(SharedPreferences.Editor editor) {
        editor.putString("AccessToken",access_token);
        editor.putString("token_type",token_type);
        editor.putString("timestamp",timestamp);
        editor.putInt("user_id",userId);
        editor.putString("role",role);
        editor.putInt("client_type",client_type);
        editor.putString("username",username);
        client.saveToPrefs(editor);
    }

    public void updateTimestamp(String timestamp, Context context){
        this.timestamp = timestamp;
        Utils.saveToPrefs("timestamp",timestamp, context);
    }

    public void restorePrefs(SharedPreferences sharedPrefs){
        access_token = sharedPrefs.getString("AccessToken","no-token");
        username = sharedPrefs.getString("token_type","00000");
        timestamp = sharedPrefs.getString("timestamp","2017-11-10 13:28:29");
        userId =  sharedPrefs.getInt("user_id",-1);
        role = sharedPrefs.getString("role","");
        client_type = sharedPrefs.getInt("client_type",2);
        username = sharedPrefs.getString("username","");
        client = new Client();
        client.restorePrefs(sharedPrefs);
    }
}
