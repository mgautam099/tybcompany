package com.wetfiretech.tybapp.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;

/**
 * @author Mayank Gautam
 *         Created: 03/11/17
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class LogWrapper implements Parcelable {

    public static final Creator<LogWrapper> CREATOR = new Creator<LogWrapper>() {
        @Override
        public LogWrapper createFromParcel(Parcel in) {
            return new LogWrapper(in);
        }

        @Override
        public LogWrapper[] newArray(int size) {
            return new LogWrapper[size];
        }
    };
    @JsonIgnore
    public Model model;
    @JsonProperty("online_id")
    public int onlineID;
    @JsonProperty("data")
    public JsonNode data;
    @JsonProperty("log")
    public Log log;

    public LogWrapper(){

    }

    protected LogWrapper(Parcel in) {
        model = in.readParcelable(Model.class.getClassLoader());
        onlineID = in.readInt();
        log = in.readParcelable(Log.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(model, i);
        parcel.writeInt(onlineID);
        parcel.writeParcelable(log, i);
    }
}
