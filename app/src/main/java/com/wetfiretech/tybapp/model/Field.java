package com.wetfiretech.tybapp.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Mayank Gautam
 *         Created: 29/09/17
 */

@Table(name = "Field")
public class Field extends Entity implements Parcelable {

    public static final Creator<Field> CREATOR = new Creator<Field>() {
        @Override
        public Field createFromParcel(Parcel in) {
            return new Field(in);
        }

        @Override
        public Field[] newArray(int size) {
            return new Field[size];
        }
    };
    @Column
    @JsonProperty("desc")
    public String desc;

    public Field(){
        super();
    }

    protected Field(Parcel in) {
        super(in);
        desc = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(desc);
    }


}
