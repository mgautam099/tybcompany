package com.wetfiretech.tybapp.model;


import android.content.SharedPreferences;
import android.os.Parcel;
import android.os.Parcelable;

import com.activeandroid.annotation.Column;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.wetfiretech.tybapp.Utils;

public class Person extends Entity implements Parcelable {

    public static final Creator<Person> CREATOR = new Creator<Person>() {
        @Override
        public Person createFromParcel(Parcel in) {
            return new Person(in);
        }

        @Override
        public Person[] newArray(int size) {
            return new Person[size];
        }
    };
    @Column(name = "address")
    @JsonProperty("address")
    public String address;
    @Column(name = "date_of_birth")
    @JsonProperty("date_of_birth")
    public String dob;
    @Column(name = "email")
    @JsonProperty("email")
    public String email;
    @Column(name = "gender")
    @JsonProperty("gender")
    public int gender;
    @Column(name = "phone_no")
    @JsonProperty("phone_no")
    public String phone_no;
    @Column(name = "image")
    @JsonProperty("image")
    public String image;
    @Column(name = "state")
    @JsonProperty("state")
    public State state;
    @Column(name = "status")
    @JsonProperty("status")
    public int status;
    @Column(name = "pincode")
    @JsonProperty("pincode")
    private Pincode pincode;

    public Person(){
        super();
    }

    protected Person(Parcel in) {
        super(in);
        address = in.readString();
        dob = in.readString();
        email = in.readString();
        gender = in.readInt();
        phone_no = in.readString();
        image = in.readString();
        state = in.readParcelable(State.class.getClassLoader());
        status = in.readInt();
        pincode = in.readParcelable(Pincode.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(address);
        dest.writeString(dob);
        dest.writeString(email);
        dest.writeInt(gender);
        dest.writeString(phone_no);
        dest.writeString(image);
        dest.writeParcelable(state, flags);
        dest.writeInt(status);
        dest.writeParcelable(pincode, flags);
    }

    @JsonProperty("pincode")
    public Pincode getPincode() {
        return pincode;
    }

    @JsonIgnore
    public void setPincode(String pincode){
        this.pincode = Pincode.getPincode(pincode);
        if(this.pincode==null){
            this.pincode = new Pincode();
            this.pincode.pincode = pincode;
            this.pincode.save();
        }
    }

    @JsonProperty("pincode")
    public void setPincode(Pincode pincode) {
        if(pincode!=null){
            this.pincode = Utils.getById(Pincode.class,pincode.eid);
            if(this.pincode == null){
                this.pincode = pincode;
                this.pincode.save();
            }
        }
    }

    @JsonProperty("pincode_id")
    public void setPincodeId(int pincode) {
        this.pincode = Utils.getById(Pincode.class,pincode);
    }

    @JsonProperty("state")
    public void setState(State state){
        if(state!=null){
            this.state = Utils.getById(State.class,state.eid);
        }
    }

    @JsonProperty("state_id")
    public void setStateId(int state){
        this.state = Utils.getById(State.class,state);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Person person = (Person) o;

        if (gender != person.gender) return false;
        if (status != person.status) return false;
        if (address != null ? !address.equals(person.address) : person.address != null)
            return false;
        if (dob != null ? !dob.equals(person.dob) : person.dob != null) return false;
        if (email != null ? !email.equals(person.email) : person.email != null) return false;
        if (phone_no != null ? !phone_no.equals(person.phone_no) : person.phone_no != null)
            return false;
        if (image != null ? !image.equals(person.image) : person.image != null) return false;
        if (state != null ? !state.equals(person.state) : person.state != null) return false;
        return pincode != null ? pincode.equals(person.pincode) : person.pincode == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (address != null ? address.hashCode() : 0);
        result = 31 * result + (dob != null ? dob.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + gender;
        result = 31 * result + (phone_no != null ? phone_no.hashCode() : 0);
        result = 31 * result + (image != null ? image.hashCode() : 0);
        result = 31 * result + (state != null ? state.hashCode() : 0);
        result = 31 * result + status;
        result = 31 * result + (pincode != null ? pincode.hashCode() : 0);
        return result;
    }

    public void saveToPrefs(SharedPreferences.Editor editor) {
        editor.putString("name",name);
        editor.putString("email",email);
        editor.putString("phone_no",phone_no);
    }

    public void restorePrefs(SharedPreferences sharedPrefs) {
        name = sharedPrefs.getString("name","");
        email = sharedPrefs.getString("email","");
        phone_no = sharedPrefs.getString("phone_no","");
    }
}
