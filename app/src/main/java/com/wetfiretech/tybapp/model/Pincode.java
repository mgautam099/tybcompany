package com.wetfiretech.tybapp.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.wetfiretech.tybapp.Utils;

/**
 * @author Mayank Gautam
 *         Created: 18/09/17
 */


@Table(name = "Pincode")
public class Pincode extends Model implements Parcelable {

    public static final Creator<Pincode> CREATOR = new Creator<Pincode>() {
        @Override
        public Pincode createFromParcel(Parcel in) {
            return new Pincode(in);
        }

        @Override
        public Pincode[] newArray(int size) {
            return new Pincode[size];
        }
    };
    @Column
    @JsonProperty("pincode")
    public String pincode;

    public Pincode(){
        super();
    }

    protected Pincode(Parcel in) {
        super(in);
        pincode = in.readString();
    }

    @JsonIgnore
    public static Pincode getPincode(String pincode){
        return new Select().from(Pincode.class).where("pincode = ?",pincode).executeSingle();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(pincode);
    }

    @JsonIgnore
    Pincode getPincode(int id){
        return Utils.getById(this.getClass(),id);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Pincode pincode1 = (Pincode) o;

        return pincode != null ? pincode.equals(pincode1.pincode) : pincode1.pincode == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (pincode != null ? pincode.hashCode() : 0);
        return result;
    }
}
