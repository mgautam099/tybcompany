package com.wetfiretech.tybapp.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.activeandroid.annotation.Table;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.wetfiretech.tybapp.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Mayank Gautam
 *         Created: 03/10/17
 */
@Table(name = "ServiceProvider")
public class ServiceProvider extends Person implements Parcelable {

    public static final Creator<ServiceProvider> CREATOR = new Creator<ServiceProvider>() {
        @Override
        public ServiceProvider createFromParcel(Parcel in) {
            return new ServiceProvider(in);
        }

        @Override
        public ServiceProvider[] newArray(int size) {
            return new ServiceProvider[size];
        }
    };
    public ServiceProvider(){
        super();
    }

    protected ServiceProvider(Parcel in) {
        super(in);
    }

    @JsonIgnore
    public static ServiceProvider getServiceProvider(int id){
        return Utils.getById(ServiceProvider.class,id);
    }

    @JsonIgnore
    public static ArrayList<ServiceProvider> getAllServiceProviders(){
        return new ArrayList<>(Utils.getAll(ServiceProvider.class));
    }


    @JsonIgnore
    public static ArrayList<ServiceProvider> getAllServiceProviders(State state){
        if(state==null){
            return getAllServiceProviders();
        }
        else
            return new ArrayList<>(state.items(ServiceProvider.class));
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
    }

    @JsonProperty("partner")
    public void setPartner(List<Partner> partner){
        for(Partner p: partner){
            ServiceProviderPartner spp = new ServiceProviderPartner();
            spp.partner = Utils.getById(p);
            if(spp.partner!=null){
                this.save();
                spp.serviceProvider = this;
                spp.save();
            }
        }
    }

    @JsonProperty("partner_id")
    public void setPartnerId(List<Integer> partners) {
        for(int p: partners){
            ServiceProviderPartner spp = new ServiceProviderPartner();
            spp.partner = Utils.getById(Partner.class,p);
            if(spp.partner!=null){
                this.save();
                spp.serviceProvider = this;
                spp.save();
            }
        }
    }


//    public List<Partner> getPartner(){
//
//    }

}
