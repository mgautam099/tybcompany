package com.wetfiretech.tybapp;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.wetfiretech.tybapp.model.LoginDetail;
import com.wetfiretech.tybapp.network.NetworkRequest;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Map;

public class TybFirebaseMsgService extends FirebaseMessagingService implements OnLoadFinishListener  {
    private static final String TAG = "MyAndroidFCMService";
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Map<String, String> map = remoteMessage.getData();
        if(map.containsKey(Utils.KEY_ORDER_ID) && map.containsKey(Utils.KEY_COMPLAINT_NUM) && map.containsKey(Utils.KEY_NOTIFICATION_MESSAGE)) {
            NetworkRequest.INSTANCE.getLog(this);
            if(NetworkRequest.INSTANCE.getLoginDetail()!=null)
                createNotification(map.get(Utils.KEY_NOTIFICATION_MESSAGE));
        }
    }

    private void createNotification( String messageBody) {
        Intent intent = new Intent( this , MainActivity. class );
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent resultIntent = PendingIntent.getActivity( this , 0, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri notificationSoundURI = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder mNotificationBuilder = new NotificationCompat.Builder( this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("TYB Complaint Update")
                .setContentText(messageBody)
                .setAutoCancel( true )
                .setSound(notificationSoundURI)
                .setContentIntent(resultIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0, mNotificationBuilder.build());
    }

    public void dispatch(Bundle extras){
        Intent intent = new Intent();
        intent.setAction(Utils.NOTIFICATION_INTENT);
        intent.putExtra(Utils.ARGS_EXTRAS,extras);
        sendBroadcast(intent);
    }

    @Override
    public void onError(@NotNull Throwable throwable) {
        Utils.logException(throwable);
    }

    @NotNull
    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void displayHomeFragment() {

    }

    @Override
    public void onLoadComplete(@Nullable Object obj, @NotNull String message) {
        Bundle bundle = new Bundle();
        bundle.putString(Utils.KEY_MODEL_UPDATE,message);
        dispatch(bundle);
    }

    @Override
    public void saveToken(@NotNull LoginDetail loginDetail) {

    }

    @Override
    public void showProgress(String message) {

    }

    @Override
    public void hideProgress() {

    }
}
