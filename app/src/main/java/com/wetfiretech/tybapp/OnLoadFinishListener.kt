package com.wetfiretech.tybapp

import android.content.Context
import com.wetfiretech.tybapp.model.LoginDetail

interface OnLoadFinishListener {
    fun displayHomeFragment()

    fun onError(throwable: Throwable)

    fun onLoadComplete(result: Any?, message: String)

    fun saveToken(loginDetail: LoginDetail)

    fun getContext(): Context

    fun showProgress(message: String)
    fun hideProgress()
}
